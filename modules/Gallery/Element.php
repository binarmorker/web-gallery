<?php

namespace Apine\Modules\Gallery;

use Apine\Entity\EntityModel;

class Element extends EntityModel {
    
    private $id;
    private $post;
    private $file;
    private $type;
    private $removed;
    private $reported;

    /**
     * Element constructor.
     * @param null $id
     */
    public function __construct($id = null) {
        $this->_initialize('obar_elements', $id);

        if ($id != null) {
            $this->id = $id;
            $this->_force_loaded();
        }
    }

    /**
     *
     */
    public function load() {
    }

    /**
     *
     */
    public function save() {
		parent::_save();
        $this->id = $this->_get_id();
    }

    /**
     *
     */
    public function delete() {
		parent::_destroy();
    }

    /**
     * @return int
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * @param $a_id
     */
    public function set_id($a_id) {
        $this->id = $a_id;
        $this->_set_field('id', $a_id);
    }

    /**
     * @return Post
     */
    public function get_post() {
        return $this->post;
    }

    /**
     * @param int|Post $a_post
     */
    public function set_post($a_post) {
        if (is_a($a_post, 'Apine\Modules\Gallery\Post')) {
            $this->post = $a_post;
            $this->_set_field('post', $a_post->get_id());
        } else {
            $this->_set_field('post', $a_post);
        }
    }

    /**
     * @return string
     */
    public function get_file() {
        return $this->file;
    }

    /**
     * @param $a_file
     */
    public function set_file($a_file) {
        $this->file = $a_file;
        $this->_set_field('file', $a_file);
    }

    /**
     * @return int
     */
    public function get_type() {
        return $this->type;
    }

    /**
     * @param $a_type
     */
    public function set_type($a_type) {
        $this->type = $a_type;
        $this->_set_field('type', $a_type);
    }

    /**
     * @return bool
     */
    public function get_removed() {
        return $this->removed;
    }

    /**
     * @param $a_removed
     */
    public function set_removed($a_removed) {
        $this->removed = $a_removed;
        $this->_set_field('removed', $a_removed);
    }

    /**
     * @return bool
     */
    public function get_reported() {
        return $this->reported;
    }

    /**
     * @param $a_reported
     */
    public function set_reported($a_reported) {
        $this->reported = $a_reported;
        $this->_set_field('reported', $a_reported);
    }
    
}