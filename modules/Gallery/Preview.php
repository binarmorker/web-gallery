<?php

namespace Apine\Modules\Gallery;

use Apine\Entity\EntityModel;

class Preview extends EntityModel {
    
    private $id;
    private $name;
    private $type;
    private $count;
    private $image;
    private $mature;
    private $removed;
    private $publication_date;

    /**
     * Preview constructor.
     * @param int|null $id
     */
    public function __construct($id = null) {
        if ($id != null) {
            $this->id = $id;
            $this->_force_loaded();
        }
    }

    /**
     *
     */
    public function load() {
    }

    /**
     *
     */
    public function save() {
    }

    /**
     *
     */
    public function delete() {
    }

    /**
     * @return int|null
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * @param int $a_id
     */
    public function set_id($a_id) {
        $this->id = $a_id;
    }

    /**
     * @return string
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * @param string $a_name
     */
    public function set_name($a_name) {
        $this->name = $a_name;
    }

    /**
     * @return string
     */
    public function get_type() {
        return $this->type;
    }

    /**
     * @param string $a_type
     */
    public function set_type($a_type) {
        $this->type = $a_type;
    }

    /**
     * @return int
     */
    public function get_count() {
        return $this->count;
    }

    /**
     * @param int $a_count
     */
    public function set_count($a_count) {
        $this->count = $a_count;
    }

    /**
     * @return string
     */
    public function get_image() {
        return $this->image;
    }

    /**
     * @param string $a_image
     */
    public function set_image($a_image) {
        $this->image = $a_image;
    }

    /**
     * @return bool
     */
    public function get_mature() {
        return $this->mature;
    }

    /**
     * @param bool $a_mature
     */
    public function set_mature($a_mature) {
        $this->mature = $a_mature;
    }

    /**
     * @return bool
     */
    public function get_removed() {
        return $this->removed;
    }

    /**
     * @param bool $a_removed
     */
    public function set_removed($a_removed) {
        $this->removed = $a_removed;
    }

    /**
     * @return string
     */
    public function get_publication_date() {
        return $this->publication_date;
    }

    /**
     * @param string $a_publication_date
     */
    public function set_publication_date($a_publication_date) {
        $this->publication_date = $a_publication_date;
    }
    
}