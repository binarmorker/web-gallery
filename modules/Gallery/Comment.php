<?php

namespace Apine\Modules\Gallery;

use Apine\Entity\EntityModel;

class Comment extends EntityModel {
    
    private $id;
    private $post;
    private $author;
    private $comment;
    private $image;
    private $upvotes;
    private $downvotes;
    private $publication_date;
    private $removed;

    /**
     * Comment constructor.
     * @param int|null $id
     */
    public function __construct($id = null) {
        $this->_initialize('obar_comments', $id);

        if ($id != null) {
            $this->id = $id;
            $this->load();
        }
    }

    /**
     *
     */
    public function load() {
        $this->_force_loaded();
    }

    /**
     *
     */
    public function save() {
		parent::_save();
        $this->id = $this->_get_id();
    }

    /**
     *
     */
    public function delete() {
		parent::_destroy();
    }

    /**
     * @return int|null
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * @param int $a_id
     */
    public function set_id($a_id) {
        $this->id = $a_id;
        $this->_set_field('id', $a_id);
    }

    /**
     * @return int
     */
    public function get_post() {
        return $this->post;
    }

    /**
     * @param int|Post $a_post
     */
    public function set_post($a_post) {
        if (is_a($a_post, 'Apine\Modules\Gallery\Post')) {
            $this->post = $a_post;
            $this->_set_field('post', $a_post->get_id());
        } else {
            $this->post = $a_post;
            $this->_set_field('post', $a_post);
        }
    }

    /**
     * @return int
     */
    public function get_author() {
        return $this->author;
    }

    /**
     * @param int $a_author
     */
    public function set_author($a_author) {
        $this->author = $a_author;
        $this->_set_field('author', $a_author);
    }

    /**
     * @return string
     */
    public function get_comment() {
        return $this->comment;
    }

    /**
     * @param string $a_comment
     */
    public function set_comment($a_comment) {
        $this->comment = $a_comment;
        $this->_set_field('comment', $a_comment);
    }

    /**
     * @return string
     */
    public function get_image() {
        return $this->image;
    }

    /**
     * @param string $a_image
     */
    public function set_image($a_image) {
        $this->image = $a_image;
        $this->_set_field('image', $a_image);
    }

    /**
     * @return int
     */
    public function get_upvotes() {
        return $this->upvotes;
    }

    /**
     * @param int $a_upvotes
     */
    public function set_upvotes($a_upvotes) {
        $this->upvotes = $a_upvotes;
    }

    /**
     * @return int
     */
    public function get_downvotes() {
        return $this->downvotes;
    }

    /**
     * @param int $a_downvotes
     */
    public function set_downvotes($a_downvotes) {
        $this->downvotes = $a_downvotes;
    }

    /**
     * @return string
     */
    public function get_publication_date() {
        return $this->publication_date;
    }

    /**
     * @param string $a_publication_date
     */
    public function set_publication_date($a_publication_date) {
        $this->publication_date = $a_publication_date;
        $this->_set_field('publication_date', $a_publication_date);
    }

    /**
     * @return bool
     */
    public function get_removed() {
        return $this->removed;
    }

    /**
     * @param bool $a_removed
     */
    public function set_removed($a_removed) {
        $this->removed = $a_removed;
        $this->_set_field('removed', $a_removed);
    }
    
}