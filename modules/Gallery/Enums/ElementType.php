<?php

namespace Apine\Modules\Gallery\Enums;

class ElementType {
    const None = 0;
    const Image = 1;
    const Video = 2;
    const Text = 3;
}