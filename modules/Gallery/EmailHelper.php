<?php
/**
 * Created by PhpStorm.
 * User: BinarMorker
 * Date: 2017-04-17
 * Time: 21:47
 */

namespace Apine\Modules\Gallery;


use Apine\Core\Encryption;
use Apine\MVC\HTMLView;
use Apine\User\PasswordToken;
use Apine\User\User;

class EmailHelper {

    public static function send_recovery_email(User $user) {
        $token = Encryption::token();
        $password_token = new PasswordToken();
        $password_token->set_user($user);
        $password_token->set_token($token);
        $username = $user->get_username();

        $email = new HTMLView('', 'email/recovery', 'null');
        $email->set_param('username', $user->get_username());
        $email->set_param('token', $token);
        $text = <<<TEXT
Hey "$username",

Was it you that asked for a password reset?

If not, please delete this email promptly!

However, if you did request a new password, please enter a new one http://obar18.com/recovery/{token}

- The O-Bar 18+ administration team.
TEXT;
        MailGunHelper::sendMail($user->get_username(), $user->get_email_address(), 'O-bar 18+ - Password reset', $text, $email->content());
        $password_token->save();
    }

}