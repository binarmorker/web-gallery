<?php

namespace Apine\Modules\Gallery;

use Apine\Application\Config;

class MailGunHelper {

    public static function sendMail($name, $email, $subject, $text, $html) {
        if (Config::get('runtime', 'mailgun_domain') &&
            Config::get('runtime', 'mailgun_key') &&
            Config::get('runtime', 'postmaster_email')) {

            $url = 'https://api.mailgun.net/v3/' . Config::get('runtime', 'mailgun_domain') . '/';
            $api_key = Config::get('runtime', 'mailgun_key');
            $from = join(" ", [
                Config::get('application', 'title'),
                Config::get('application', 'subtitle'),
                '<' . Config::get('runtime', 'postmaster_email') . '>'
            ]);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HEADER, [
                'api' => $api_key
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'from' => $from,
                'to' => $name . ' <' . $email . '>',
                'subject' => $subject,
                'text' => $text,
                'html' => $html
            ]);

            curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            return $status;
        } else {
            return 200;
        }
    }

}