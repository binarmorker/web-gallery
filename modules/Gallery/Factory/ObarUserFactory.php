<?php

namespace Apine\Modules\Gallery\Factory;

use Apine\Core\Collection;
use Apine\Core\Database;
use Apine\Entity\EntityFactoryInterface;
use Apine\User\Factory\UserFactory;
use Apine\User\User;
use Apine\User\UserGroup;
use Identicon\Identicon;

class ObarUserFactory implements EntityFactoryInterface {

    // Unused
    public static function create_all() {}
    public static function create_by_id($a_id) {}

    /**
     * @param $a_page
     * @param $a_count
     * @return \Apine\User\User[]
     */
    public static function create_paginated($a_page, $a_count) {
        $database = new Database();
        $page = intval(($a_page - 1) * $a_count);
        $count = intval($a_count);
        $query = "SELECT `id` FROM `apine_users`
                  ORDER BY `register`
                  LIMIT $count OFFSET $page;";
        $results = $database->select($query);
        $users = array();

        foreach ($results as $result) {
            $user = UserFactory::create_by_id($result['id']);

            if ($user->get_property('avatar') == null) {
                $identicon = new Identicon();
                $image = $identicon->getImageDataUri($user->get_email_address(), 250);
                $user->set_property('avatar', $image);
                $user->save();
            }

            $users[] = $user;
        }

        return $users;
    }

    /**
     * @param $a_page
     * @param $a_count
     * @param $a_user
     * @return array
     */
    public static function create_notifications($a_page, $a_count, $a_user) {
        $database = new Database();
        $page = intval(($a_page - 1) * $a_count);
        $count = intval($a_count);
        $query = "SELECT * FROM `obar_notifications`
                  WHERE `user_id` = $a_user
                  AND (CASE
                    WHEN `obar_notifications`.`type` = 1 
                    OR `obar_notifications`.`type` = 2 
                    OR `obar_notifications`.`type` = 3 
                    OR `obar_notifications`.`type` = 4 
                    OR `obar_notifications`.`type` = 5 
                    OR `obar_notifications`.`type` = 6 
                    THEN (SELECT `obar_posts`.`removed`
                      FROM `obar_posts`
                      WHERE `obar_posts`.`id` = `obar_notifications`.`publication_id`)
                    ELSE 1
                  END) = 0
                  ORDER BY `date` DESC
                  LIMIT $count OFFSET $page;";
        $results = $database->select($query);
        return $results;
    }

    /**
     * @return int
     */
    public static function get_count() {
        $database = new Database();
        $query = "SELECT COUNT(`id`) AS `count` FROM `apine_users`;";
        $totalMembers = $database->select($query)[0]['count'];
        return intval($totalMembers);
    }

    /**
     * @param $a_user
     * @return int
     */
    public static function get_notification_count($a_user) {
        $database = new Database();
        $query = "SELECT COUNT(*) AS `count`
                  FROM `obar_notifications` 
                  WHERE `obar_notifications`.`user_id` = $a_user
                  AND (CASE
                    WHEN `obar_notifications`.`type` = 1 
                    OR `obar_notifications`.`type` = 2 
                    OR `obar_notifications`.`type` = 3 
                    OR `obar_notifications`.`type` = 4 
                    OR `obar_notifications`.`type` = 5 
                    OR `obar_notifications`.`type` = 6 
                    THEN (SELECT `obar_posts`.`removed`
                      FROM `obar_posts`
                      WHERE `obar_posts`.`id` = `obar_notifications`.`publication_id`)
                    ELSE 1
                  END) = 0;";
        $totalMembers = $database->select($query)[0]['count'];
        return intval($totalMembers);
    }

    /**
     * @param $a_user
     * @return int
     */
    public static function get_unread_notification_count($a_user) {
        $database = new Database();
        $query = "SELECT COUNT(*) AS `count` 
                  FROM `obar_notifications` 
                  WHERE `user_id` = $a_user 
                  AND `seen` = 0
                  AND (CASE
                    WHEN `obar_notifications`.`type` = 1 
                    OR `obar_notifications`.`type` = 2 
                    OR `obar_notifications`.`type` = 3 
                    OR `obar_notifications`.`type` = 4 
                    OR `obar_notifications`.`type` = 5 
                    OR `obar_notifications`.`type` = 6 
                    THEN (SELECT `obar_posts`.`removed`
                      FROM `obar_posts`
                      WHERE `obar_posts`.`id` = `obar_notifications`.`publication_id`)
                    ELSE 1
                  END) = 0;";
        $totalMembers = $database->select($query)[0]['count'];
        return intval($totalMembers);
    }

    /**
     * @param Collection $groups
     * @return UserGroup
     */
    public static function getHighestRank(Collection $groups) {
        $array = $groups->get_all();
        usort($array, function(UserGroup $a, UserGroup $b) {
            return $a->get_id() < $b->get_id();
        });
        return $array[0];
    }

    public static function isAdmin($user) {
        $is_admin = false;

        if (!is_null($user) && $user instanceof User) {
            $wants_admin = $user->get_property('admin');
            $is_admin = $user->has_group(3) && $wants_admin == 'true';
        }

        return $is_admin;
    }

    public static function read_all_notifications($user_id) {
        $database = new Database();
        $database->update('obar_notifications', array('seen' => 1), array('user_id' => $user_id));
    }

    public static function read_notification($publication_id, $type, $user_id) {
        $database = new Database();
        $database->update('obar_notifications', array('seen' => 1), array('publication_id' => $publication_id, 'type' => $type, 'user_id' => $user_id));
    }

    public static function unread_notification($publication_id, $type, $user_id) {
        $database = new Database();
        $database->update('obar_notifications', array('seen' => 0), array('publication_id' => $publication_id, 'type' => $type, 'user_id' => $user_id));
    }

}