<?php

namespace Apine\Modules\Gallery\Factory;

use Apine\Core\Database;
use Apine\Entity\EntityFactoryInterface;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Element;
use Apine\Modules\Gallery\Post;
use Apine\Modules\Gallery\Preview;
use Apine\Session\SessionManager;
use DateTime;

class PostFactory implements EntityFactoryInterface {

    /**
     * Checks if the Post exists
     * @param int $a_id
     * @param bool $is_admin
     * @return bool
     */
    public static function is_id_exist($a_id, $is_admin = false) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT `id` FROM `obar_posts` WHERE `id` = $id";

        if (!$is_admin) {
            $query .= " AND `removed` = 0";
        }

        $response = $database->select($query);

        if ($response) {
            return true;
        }
        
        return false;
    }

    /**
     * @param array $params
     * @throws GenericException
     */
    public static function create_all($params = array()) {
        throw new GenericException("Not implemented", 400);
    }

    /**
     * Returns the previous post's id
     * @param int $a_id
     * @param $a_mature
     * @param $a_user
     * @param $is_admin
     * @return bool
     */
    public static function get_previous_id($a_id, $a_mature, $a_user, $is_admin) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT `id` FROM `obar_posts` WHERE `id` < $id";

        if (!$is_admin) {
            $query .= " AND `removed` = 0";
        }

        if (!$a_mature) {
            $query .= " AND `mature` = 0";
        }

        if ($a_user) {
            $query .= " AND `author` = $a_user";
        }

        $query .= " ORDER BY `id` DESC LIMIT 1;";

        $response = $database->select($query);

        if ($response) {
            return $response[0]['id'];
        }

        return null;
    }

    /**
     * Returns the next post's id
     * @param int $a_id
     * @param $a_mature
     * @param $a_user
     * @param $is_admin
     * @return bool
     */
    public static function get_next_id($a_id, $a_mature, $a_user, $is_admin) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT `id` FROM `obar_posts` WHERE `id` > $id";

        if (!$is_admin) {
            $query .= " AND `removed` = 0";
        }

        if (!$a_mature) {
            $query .= " AND `mature` = 0";
        }

        if ($a_user) {
            $query .= " AND `author` = $a_user";
        }

        $query .= " ORDER BY `id` ASC LIMIT 1;";

        $response = $database->select($query);

        if ($response) {
            return $response[0]['id'];
        }

        return null;
    }

    /**
     * Creates a Post
     * @param int $a_id
     * @param array $params
     * @return Post|null
     */
    public static function create_by_id($a_id, $params = array()) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT `obar_posts`.`id`, 
                         `obar_posts`.`name`, 
                         `obar_posts`.`description`, 
                         `obar_posts`.`author`,
                         `obar_posts`.`publication_date`, 
                         `obar_posts`.`mature`,
                         `obar_posts`.`removed`, 
                         `obar_elements`.`id` AS `element_id`, 
                         `obar_elements`.`file`, 
                         `obar_elements`.`type`,
                         `obar_elements`.`removed` AS `element_removed`, 
                         `obar_elements`.`reported`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = $id
                          AND `obar_votes`.`type` = 1
                          AND `obar_votes`.`upvote` = 1) AS `upvotes`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = $id
                          AND `obar_votes`.`type` = 1
                          AND `obar_votes`.`upvote` = 0) AS `downvotes`
                  FROM `obar_posts` 
                  LEFT JOIN `obar_elements` ON `obar_posts`.`id` = `obar_elements`.`post`
                  WHERE `obar_posts`.`id` = $id";

        if (!isset($params['is_admin']) || !$params['is_admin']) {
            $query .= " AND `obar_posts`.`removed` = 0 AND `obar_elements`.`removed` = 0";
        }

        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            $post = new Post((int)$response[0]['id']);
            $post->set_name($response[0]['name']);
            $post->set_description($response[0]['description']);
            $post->set_author($response[0]['author']);

            $datetime = new DateTime('now');
            $time = strtotime($response[0]['publication_date']);
            $time += $datetime->getOffset();
            $value = date("Y-m-d H:i:s", $time);
            $post->set_publication_date($value);

            $post->set_upvotes((int)$response[0]['upvotes']);
            $post->set_downvotes((int)$response[0]['downvotes']);
            $post->set_mature((bool)$response[0]['mature']);
            $post->set_removed((bool)$response[0]['removed']);
            $elements = array();

            foreach ($response as $item) {
                $element = new Element((int)$item['element_id']);
                $element->set_post($post);
                $type = explode('/', $item['type']);

                switch ($type[0]) {
                    case 'facebook':
                        $filename = explode('/', $item['file']);

                        if ($type[1] == 'video') {
                            $element->set_file("https://www.facebook.com/{$filename[0]}/videos/{$filename[1]}/");
                        } else {
                            $element->set_file("https://www.facebook.com/{$filename[0]}/posts/{$filename[1]}/");
                        }

                        break;
                    case 'image':
                    case 'video':
                        $element->set_file('/media/' . $item['file']);
                        break;
                    case 'url':
                    default:
                        $element->set_file($item['file']);
                        break;
                }

                $element->set_type($item['type']);
                $element->set_removed((bool)$item['removed']);
                $element->set_reported((bool)$item['reported']);
                $elements[] = $element;
            }

            $post->set_elements($elements);
            return $post;
        } else {
            return null;
        }
    }

    /**
     * Creates Previews for every post
     * @param int $count
     * @param null $a_last_date
     * @param bool $mature
     * @param bool $is_admin
     * @return Preview[]
     */
    public static function create_previews($is_admin, $count = 50, $a_last_date = null, $mature = false) {
        $database = new Database();

        $query = "SELECT `obar_posts`.`id`,
                         `obar_posts`.`name`,
                         `obar_elements`.`file`,
                         `obar_elements`.`type`,
                         `obar_posts`.`mature`,
                         `obar_posts`.`removed`,
                         `obar_posts`.`publication_date`,
                         (SELECT COUNT(*) 
                          FROM `obar_elements` 
                          WHERE `obar_elements`.`post` = `obar_posts`.`id`) AS `count`
                  FROM `obar_posts`
                  LEFT JOIN `obar_elements`
                  ON `obar_elements`.`id` = (SELECT `obar_elements`.`id`
                                             FROM `obar_elements`
                                             WHERE `obar_elements`.`post` = `obar_posts`.`id`
                                             ORDER BY `obar_elements`.`id`
                                             LIMIT 1)
                  WHERE 1";

        if (!$is_admin) {
            $query .= " AND `obar_posts`.`removed` = 0";
        }

        if (!SessionManager::get_instance()->is_logged_in() || !$mature) {
            $query .= " AND `obar_posts`.`mature` = 0";
        }

        if (!is_null($a_last_date)) {
            $last_date = $database->quote($a_last_date);
            $query .= " AND `obar_posts`.`publication_date` < $last_date";
        }

        $query .= " ORDER BY `obar_posts`.`publication_date` DESC
                   LIMIT $count";

        $response = $database->select($query);
        $previews = array();
        
        if (isset($response)) {
            foreach ($response as $item) {
                $preview = new Preview((int)$item['id']);
                $preview->set_name($item['name']);
                $filename = pathinfo($item['file'])['filename'];
                $preview->set_type($item['type']);
                $preview->set_count($item['count']);

                switch (explode('/', $item['type'])[0]) {
                    case 'facebook':
                        $filename = explode('/', $item['file']);
                        $preview->set_image("https://graph.facebook.com/{$filename[1]}/picture");
                        break;
                    case 'image':
                    case 'video':
                        $preview->set_image('/media/' . 'thumb_' . $filename . '.jpg');
                        break;
                    case 'url':
                    default:
                        $preview->set_image($item['file']);
                        break;
                }

                $preview->set_mature($item['mature'] == "1");
                $preview->set_removed($item['removed'] == "1");

                $datetime = new DateTime('now');
                $time = strtotime($item['publication_date']);
                $time += $datetime->getOffset();
                $value = date("Y-m-d H:i:s", $time);
                $preview->set_publication_date($value);
                $previews[] = $preview;
            }
        }
        
        return $previews;
    }

    /**
     * Creates Previews for every post
     * @param $user
     * @param $is_admin
     * @param int $count
     * @param null $a_last_date
     * @param bool $mature
     * @return Preview[]
     */
    public static function create_previews_for_user($user, $is_admin, $count = 50, $a_last_date = null, $mature = false) {
        $database = new Database();

        $query = "SELECT `obar_posts`.`id`,
                         `obar_posts`.`name`,
                         `obar_elements`.`file`,
                         `obar_elements`.`type`,
                         `obar_posts`.`mature`,
                         `obar_posts`.`removed`,
                         `obar_posts`.`publication_date`,
                         (SELECT COUNT(*) 
                          FROM `obar_elements` 
                          WHERE `obar_elements`.`post` = `obar_posts`.`id`) AS `count`
                  FROM `obar_posts`
                  LEFT JOIN `obar_elements`
                  ON `obar_elements`.`id` = (SELECT `obar_elements`.`id`
                                             FROM `obar_elements`
                                             WHERE `obar_elements`.`post` = `obar_posts`.`id`
                                             ORDER BY `obar_elements`.`id`
                                             LIMIT 1)
                  WHERE `obar_posts`.`author` = $user";

        if (!$is_admin) {
            $query .= " AND `obar_posts`.`removed` = 0";
        }

        if (!SessionManager::get_instance()->is_logged_in() || !$mature) {
            $query .= " AND `obar_posts`.`mature` = 0";
        }

        if (!is_null($a_last_date)) {
            $last_date = $database->quote($a_last_date);
            $query .= " AND `obar_posts`.`publication_date` < $last_date";
        }

        $query .= " ORDER BY `obar_posts`.`publication_date` DESC
                   LIMIT $count";

        $response = $database->select($query);
        $previews = array();

        if (isset($response)) {
            foreach ($response as $item) {
                $preview = new Preview((int)$item['id']);
                $preview->set_name($item['name']);
                $filename = pathinfo($item['file'])['filename'];
                $preview->set_type($item['type']);
                $preview->set_count($item['count']);

                switch (explode('/', $item['type'])[0]) {
                    case 'facebook':
                        $filename = explode('/', $item['file']);
                        $preview->set_image("https://graph.facebook.com/{$filename[1]}/picture");
                        break;
                    case 'image':
                    case 'video':
                        $preview->set_image('/media/' . 'thumb_' . $filename . '.jpg');
                        break;
                    case 'url':
                    default:
                        $preview->set_image($item['file']);
                        break;
                }

                $preview->set_mature($item['mature'] == "1");
                $preview->set_removed($item['removed'] == "1");
                $preview->set_publication_date($item['publication_date']);
                $previews[] = $preview;
            }
        }

        return $previews;
    }

    /**
     * Gets the number of Posts written by an User
	 * @param $a_user_id
     * @return int|null
     */
    public static function get_user_post_count($a_user_id) {
        $database = new Database();
        $user_id = $database->quote($a_user_id);
        $query = "SELECT COUNT(*) AS `count`
                  FROM `obar_posts`
                  WHERE `author` = $user_id
                  AND `removed` = 0";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            return $response[0]['count'];
        } else {
            return null;
        }
    }

    /**
     * Gets the total number of Posts
     * @return int|null
     */
    public static function get_count() {
        $database = new Database();
        $query = "SELECT COUNT(*) AS `count`
                  FROM `obar_posts`
                  WHERE `removed` = 0";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            return $response[0]['count'];
        } else {
            return null;
        }
    }

    /**
     * Gets the reputation given by the User
	 * @param $a_user_id
     * @return int|null
     */
    public static function get_user_given_reputation($a_user_id) {
        $database = new Database();
        $user_id = $database->quote($a_user_id);
        $query = "SELECT COUNT(*) AS `total`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `author` = $user_id
                          AND `upvote` = 1) AS `upvotes`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `author` = $user_id
                          AND `upvote` = 0) AS `downvotes`
                  FROM `obar_votes`
                  WHERE `author` = $user_id";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            return $response[0];
        } else {
            return null;
        }
    }

    /**
     * Get the reputation given to posts for an user
	 * @param $a_user_id
     * @return int|null
     */
    public static function get_user_received_reputation($a_user_id) {
        $database = new Database();
        $user_id = $database->quote($a_user_id);
        $query = "SELECT COUNT(*) AS `total`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          JOIN `obar_posts` 
                          ON `obar_posts`.`id` = `obar_votes`.`post`
                          AND `obar_votes`.`type` = 1
                          AND `obar_posts`.`author` = $user_id
                          JOIN `obar_comments` 
                          ON `obar_comments`.`id` = `obar_votes`.`post`
                          AND `obar_votes`.`type` = 2
                          AND `obar_posts`.`author` = $user_id
                          WHERE `obar_votes`.`upvote` = 1) AS `upvotes`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          JOIN `obar_posts` 
                          ON `obar_posts`.`id` = `obar_votes`.`post`
                          AND `obar_votes`.`type` = 1
                          AND `obar_posts`.`author` = $user_id
                          JOIN `obar_comments` 
                          ON `obar_comments`.`id` = `obar_votes`.`post`
                          AND `obar_votes`.`type` = 2
                          AND `obar_posts`.`author` = $user_id
                          WHERE `obar_votes`.`upvote` = 0) AS `downvotes`
                  FROM `obar_votes`
                  JOIN `obar_posts` 
                  ON `obar_posts`.`id` = `obar_votes`.`post`
                  WHERE `obar_posts`.`author` = $user_id";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            return $response[0];
        } else {
            return null;
        }
    }

    /**
     * Get the number of votes for a post
     * @param int $a_type
     * @param int $a_post_id
     * @param int $a_user_id
     * @return int|null
     */
    public static function get_user_vote_for_post($a_type, $a_post_id, $a_user_id) {
        $database = new Database();
        $post_id = $database->quote($a_post_id);
        $user_id = $database->quote($a_user_id);
        $type = intval($a_type);
        $query = "SELECT `upvote`
                  FROM `obar_votes`
                  WHERE `post` = $post_id
                  AND `type` = $type
                  AND `author` = $user_id";
        $response = $database->select($query);
        
        if (isset($response) && isset($response[0])) {
            return $response[0]['upvote'];
        } else {
            return null;
        }
    }

    /**
     * Get the total number of votes for a post
     * @param int $a_post_id
     * @return array
     */
    public static function get_votes_for_post($a_post_id) {
        $database = new Database();
        $post_id = $database->quote($a_post_id);
        $query = "SELECT *
                  FROM `obar_votes`
                  WHERE `post` = $post_id
                  AND `type` = 1
                  ORDER BY `upvote` DESC, `date`";
        $response = $database->select($query);

        return $response;
    }

    /**
     * Get the total number of votes for a comment
     * @param int $a_post_id
     * @return array
     */
    public static function get_votes_for_comment($a_post_id) {
        $database = new Database();
        $post_id = $database->quote($a_post_id);
        $query = "SELECT *
                  FROM `obar_votes`
                  WHERE `post` = $post_id
                  AND `type` = 2
                  ORDER BY `upvote` DESC, `date`";
        $response = $database->select($query);

        return $response;
    }

    /**
     * Adds a vote for the Post
     * @param int|null $a_vote
     * @param int $a_type
     * @param int $a_post_id
     * @param int $a_user_id
     * @return int
     */
    public static function apply_vote($a_type, $a_vote, $a_post_id, $a_user_id) {
        $database = new Database();

        if ($a_vote !== null) {
            $vote = $database->quote($a_vote);
            $post_id = $database->quote($a_post_id);
            $user_id = $database->quote($a_user_id);
            $type = intval($a_type);
            $query = "INSERT INTO `obar_votes` (`post`, `type`, `author`, `upvote`)
                      VALUES ($post_id, $type, $user_id, $vote)
                      ON DUPLICATE KEY
                      UPDATE `upvote` = $vote";
            $response = $database->exec($query);
        } else {
            $post_id = $database->quote($a_post_id);
            $user_id = $database->quote($a_user_id);
            $type = intval($a_type);
            $query = "DELETE FROM `obar_votes`
                      WHERE `author` = $user_id
                      AND `type` = $type
                      AND `post` = $post_id";
            $response = $database->exec($query);
        }
        
        return $response;
    }
}