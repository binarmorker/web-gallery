<?php

namespace Apine\Modules\Gallery\Factory;

use Apine\Core\Database;
use Apine\Entity\EntityFactoryInterface;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Report;

class ReportFactory implements EntityFactoryInterface {

    /**
     * Checks if the Post exists
     * @param int $a_id
     * @return bool
     */
    public static function is_id_exist($a_id) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT `id` FROM `obar_reports` WHERE `id` = $id";

        $response = $database->select($query);

        if ($response) {
            return true;
        }
        
        return false;
    }

    /**
     * @param array $params
     * @return Report[]|null
     * @throws GenericException
     */
    public static function create_all($params = array()) {
        $database = new Database();
        $query = "SELECT *
                  FROM `obar_reports`";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            $reports = array();

            foreach ($response as $item) {
                $report = new Report($item['id']);
                $report->set_author($item['author']);
                $report->set_description($item['description']);
                $report->set_item_id($item['item_id']);
                $report->set_publication_date($item['publication_date']);
                $report->set_resolved($item['resolved']);
                $report->set_type($item['type']);

                switch ($item['type']) {
                    case PublicationType::Post:
                        $report->set_publication(PostFactory::create_by_id($item['item_id']));
                        break;
                    case PublicationType::Comment:
                        $report->set_publication(CommentFactory::create_by_id($item['item_id']));
                        break;
                }

                $reports[] = $report;
            }

            return $reports;
        } else {
            return null;
        }
    }

    /**
     * Creates a Post
     * @param int $a_id
     * @param array $params
     * @return Report|null
     */
    public static function create_by_id($a_id, $params = array()) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT *
                  FROM `obar_reports`
                  WHERE `obar_reports`.`id` = $id";

        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            $report = new Report($response[0]['id']);
            $report->set_author($response[0]['author']);
            $report->set_description($response[0]['description']);
            $report->set_item_id($response[0]['item_id']);
            $report->set_publication_date($response[0]['publication_date']);
            $report->set_resolved($response[0]['resolved']);
            $report->set_type($response[0]['type']);

            return $report;
        } else {
            return null;
        }
    }

    /**
     * Gets the total number of Posts
     * @return int|null
     */
    public static function get_count() {
        $database = new Database();
        $query = "SELECT COUNT(*) AS `count`
                  FROM `obar_reports`";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            return $response[0]['count'];
        } else {
            return null;
        }
    }
}