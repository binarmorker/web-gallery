<?php

namespace Apine\Modules\Gallery\Factory;

use Apine\Core\Database;
use Apine\Entity\EntityFactoryInterface;
use Apine\Modules\Gallery\Comment;
use Apine\Exception\GenericException;
use DateTime;

class CommentFactory implements EntityFactoryInterface {

    /**
     * Checks if the Comment exists
     * @param int $a_id
     * @param bool $is_admin
     * @return bool
     */
    public static function is_id_exist($a_id, $is_admin = false) {
        $database = new Database();
        $id = $database->quote($a_id);
        $query = "SELECT `id` FROM `obar_comments` WHERE `id` = $id";

        if (!$is_admin) {
            $query .= " AND `removed` = 0";
        }

        $response = $database->select($query);
        
        if ($response) {
            return true;
        }
        
        return false;
    }

    /**
     * @param array $params
     * @throws GenericException
     */
    public static function create_all($params = array()) {
        throw new GenericException("Not implemented", 400);
    }

    /**
     * @param string $a_id
     * @param array $params
     * @return Comment|null
     * @throws GenericException
     */
    public static function create_by_id($a_id, $params = array()) {
        $database = new Database();
        $query = "SELECT `post`,
                         `comment`,
                         `author`,
                         `image`,
                         `publication_date`,
                         `removed`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = `id`
                          AND `obar_votes`.`type` = 2
                          AND `obar_votes`.`upvote` = 1) AS `upvotes`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = `id`
                          AND `obar_votes`.`type` = 2
                          AND `obar_votes`.`upvote` = 0) AS `downvotes`
                  FROM `obar_comments`
                  WHERE `id` = $a_id";

        if (!isset($params['is_admin']) || !$params['is_admin']) {
            $query .= " AND `removed` = 0";
        }

        $response = $database->select($query);

        if (isset($response) && count($response) > 0) {
            $item = $response[0];
            $comment = new Comment($a_id);
            $comment->set_post((int)$item['post']);
            $comment->set_comment($item['comment']);
            $comment->set_author((int)$item['author']);
            $comment->set_upvotes((int)$item['upvotes']);
            $comment->set_downvotes((int)$item['downvotes']);

            if (!is_null($item['image'])) {
                $comment->set_image($item['image']);
            }

            $datetime = new DateTime('now');
            $time = strtotime($item['publication_date']);
            $time += $datetime->getOffset();
            $value = date("Y-m-d H:i:s", $time);
            $comment->set_publication_date($value);
            $comment->set_removed((bool)$item['removed']);

            return $comment;
        }

        return null;
    }

    /**
     * Creates all Comments for a Post
     * @param int $a_id
     * @param $params
     * @return Comment[]
     */
    public static function create_for_post($a_id, $params) {
        $database = new Database();
        $desc = isset($params['desc']) && $params['desc'] ? 'DESC' : '';
        $query = "SELECT `id`,
                         `comment`,
                         `author`,
                         `image`,
                         `publication_date`,
                         `removed`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = `id`
                          AND `obar_votes`.`type` = 2
                          AND `obar_votes`.`upvote` = 1) AS `upvotes`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = `id`
                          AND `obar_votes`.`type` = 2
                          AND `obar_votes`.`upvote` = 0) AS `downvotes`
                  FROM `obar_comments`
                  WHERE `post` = $a_id";

        if (!isset($params['is_admin']) || !$params['is_admin']) {
            $query .= " AND `removed` = 0";
        }

        $query .= " ORDER BY `publication_date` $desc";
        $response = $database->select($query);
        $comments = array();
        
        if (isset($response)) {
            foreach ($response as $item) {
                $comment = new Comment((int)$item['id']);
                $comment->set_post($a_id);
                $comment->set_comment($item['comment']);
                $comment->set_author((int)$item['author']);
                $comment->set_upvotes((int)$item['upvotes']);
                $comment->set_downvotes((int)$item['downvotes']);

                if (!is_null($item['image'])) {
                    $comment->set_image('/media/' . $item['image']);
                }

                $datetime = new DateTime('now');
                $time = strtotime($item['publication_date']);
                $time += $datetime->getOffset();
                $value = date("Y-m-d H:i:s", $time);
                $comment->set_publication_date($value);

                $comment->set_removed((bool)$item['removed']);
                $comments[] = $comment;
            }
        }
        
        return $comments;
    }

    /**
     * Creates the total number of Comments
     * @return int|null
     */
    public static function get_count() {
        $database = new Database();
        $query = "SELECT COUNT(*) AS `count`
                  FROM `obar_comments`
                  WHERE `removed` = 0";
        $response = $database->select($query);

        if (isset($response) && isset($response[0])) {
            return $response[0]['count'];
        } else {
            return null;
        }
    }

    /**
     * Creates recent Comments
     * @param $count
     * @return Comment[]
     */
    public static function create_recent($count) {
        $database = new Database();
        $query = "SELECT `id`,
                         `comment`,
                         `author`,
                         `image`,
                         `post`,
                         `publication_date`,
                         `removed`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = `id`
                          AND `obar_votes`.`type` = 2
                          AND `obar_votes`.`upvote` = 1) AS `upvotes`,
                         (SELECT COUNT(*)
                          FROM `obar_votes`
                          WHERE `obar_votes`.`post` = `id`
                          AND `obar_votes`.`type` = 2
                          AND `obar_votes`.`upvote` = 0) AS `downvotes`
                  FROM `obar_comments`
                  WHERE `removed` = 0
                  AND (SELECT `mature` FROM `obar_posts` WHERE `id` = `post`) = 0
                  ORDER BY `publication_date` DESC
                  LIMIT $count";
        $response = $database->select($query);
        $comments = array();

        if (isset($response)) {
            foreach ($response as $item) {
                $comment = new Comment((int)$item['id']);
                $comment->set_post($item['post']);
                $comment->set_comment($item['comment']);
                $comment->set_author((int)$item['author']);
                $comment->set_upvotes((int)$item['upvotes']);
                $comment->set_downvotes((int)$item['downvotes']);

                if (!is_null($item['image'])) {
                    $comment->set_image('/media/' . $item['image']);
                }

                $datetime = new DateTime('now');
                $time = strtotime($item['publication_date']);
                $time += $datetime->getOffset();
                $value = date("Y-m-d H:i:s", $time);
                $comment->set_publication_date($value);

                $comment->set_removed((bool)$item['removed']);
                $comments[] = $comment;
            }
        }

        return $comments;
    }

}