<?php

namespace Apine\Controllers\User;

use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;

class MembersController extends Controller implements APIActionsInterface {

    // WEB

    public function index() {
        $view = new TwigView();
        $view->set_view('members');
        $view->set_param('is_admin', ObarUserFactory::isAdmin(SessionManager::get_user()));
        return $view;
    }

    // API

    public function get($params) {
        $view = new JSONView();
        $response['members'] = array();

        if (isset($params[0])) {
            $page = $params[0];

            $members = ObarUserFactory::create_paginated($page, 30);

            if ($members != null) {
                foreach ($members as $item) {
                    $member = array(
                        'username' => $item->get_username(),
                        'register_date' => $item->get_register_date(),
                        'group' => ObarUserFactory::getHighestRank($item->get_group())->get_name()
                    );

                    $member['realname'] = !is_null($item->get_property('realname')) ? $item->get_property('realname') : '';
                    $member['avatar'] = $item->get_property('avatar');
                    $member['cover'] = !is_null($item->get_property('cover')) ? $item->get_property('cover') : '';

                    if (explode(':', $member['avatar'])[0] != 'data') {
                        $member['avatar'] = URLHelper::resource($member['avatar']);
                    }

                    $response['members'][] = $member;
                }

                $response['count'] = ObarUserFactory::get_count();
            }
        }

        $view->set_json_file($response);
        $view->set_response_code(200);
        return $view;
    }

    public function post($params) {
        throw new GenericException("Method Not Allowed", 405);
    }

    public function put($params) {
        throw new GenericException("Method Not Allowed", 405);
    }

    public function delete($params) {
        throw new GenericException("Method Not Allowed", 405);
    }
}