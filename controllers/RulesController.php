<?php

namespace Apine\Controllers\User;

use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\Modules\Gallery\Report;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\Session\SessionManager;

class RulesController extends Controller implements APIActionsInterface {
	
	// WEB
	
	public function index() {
        $view = new TwigView();
		$view->set_layout('layout');
		$view->set_view('rules');
		return $view;
	}
	
	// API
	
	public function get($params) {
		$view = new JSONView();
		$response['terms'] = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/resources/public/assets/termsandconditions.md');
		$response['privacy'] = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/resources/public/assets/privacypolicy.md');
		$view->set_json_file($response);
		$view->set_response_code(200);
		return $view;
	}
	
	public function post($params) {
        $view = new JSONView();

        if (SessionManager::get_instance()->is_logged_in()) {
            $user = SessionManager::get_instance()->get_user()->get_id();
        } else {
            $user = Request::server()['REMOTE_ADDR'];
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        if (!is_int($params['item_id'])) {
            $params['item_id'] = PseudoCrypt::unhash($params['item_id']);
        }

        if ($params['item_id'] != null) {
            $report = new Report();
            $report->set_type($params['type']);
            $report->set_item_id($params['item_id']);
            $report->set_author($user);
            $report->set_description($params['description']);
            $report->save();

            /*$email = new HTMLView('', 'email/report', 'null');
            $email->set_param('reportType', $params['type']);
            $text = <<<TEXT
Hey "{$params['user']}",

Thank you very much for registering on O-bar 18+.

We are pleased to see you in our awesome community!

Please join us at http://obar18.com

- The O-Bar 18+ administration team.
TEXT;
            MailGunHelper::sendMail($params['user'], $params['email'], 'Thanks for registering on O-bar 18+', $text, $email->content());*/
        } else {
            throw new GenericException("Bad request", 400);
        }

        $view->set_json_file(array("message" => "Ok"));
        $view->set_response_code(200);
        return $view;
	}
	
	public function put($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}