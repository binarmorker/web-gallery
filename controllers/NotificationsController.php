<?php

namespace Apine\Controllers\User;

use Apine\Application\Config;
use Apine\Core\Database;
use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Comment;
use Apine\Modules\Gallery\Enums\NotificationType;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\Modules\Gallery\SubscriptionHelper;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;
use Apine\User\Factory\UserFactory;
use DateTime;

class NotificationsController extends Controller implements APIActionsInterface {

    // WEB

    public function index() {
        $view = new TwigView();

        if (!SessionManager::is_logged_in()) {
            throw new GenericException('Forbidden', 403);
        }

        $view->set_view('notifications');
        $view->set_param('is_admin', ObarUserFactory::isAdmin(SessionManager::get_user()));
        return $view;
    }

    public function sync() {
        $view = new JSONView();

        if (!SessionManager::is_logged_in() || !ObarUserFactory::isAdmin(SessionManager::get_user())) {
            throw new GenericException("Forbidden", 403);
        }

        $subscriptions = array();
        $notifications = array();
        $database = new Database();

        //// Subscriptions

        // Posts
        $query = "SELECT `id`, `author` FROM `obar_posts` WHERE `removed` = 0 ORDER BY `publication_date`;";
        $results = $database->select($query);

        foreach ($results as $post) {
            $id = $post['id'];
            $type1 = PublicationType::Post;
            $type2 = PublicationType::Comment;
            $author = $post['author'];

            $result = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `user` = $author 
                                     AND `type` = $type1 
                                     AND `publication` = $id");

            if (count($result) == 0) {
                // Create subscription
                $database->insert('obar_subscriptions', array(
                    'user' => $author,
                    'type' => $type1,
                    'publication' => $id));
                $subscriptions[] = $post;
            }

            $result = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `user` = $author 
                                     AND `type` = $type2 
                                     AND `publication` = $id");

            if (count($result) == 0) {
                // Create subscription
                $database->insert('obar_subscriptions', array(
                    'user' => $author,
                    'type' => $type2,
                    'publication' => $id));
                $subscriptions[] = $post;
            }
        }

        // Comments
        $query = "SELECT `post`, `author` FROM `obar_comments` WHERE `removed` = 0 ORDER BY `publication_date`;";
        $results = $database->select($query);

        foreach ($results as $comment) {
            $id = $comment['post'];
            $type = PublicationType::Comment;
            $author = $comment['author'];

            $result = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `user` = $author 
                                     AND `type` = $type 
                                     AND `publication` = $id");

            if (count($result) == 0) {
                // Create subscription
                $database->insert('obar_subscriptions', array(
                    'user' => $author,
                    'type' => $type,
                    'publication' => $id));
                $subscriptions[] = $comment;
            }
        }
        //// End Subscriptions

        //// Notifications
        $query = "SELECT `user`, `type`, `publication` FROM `obar_subscriptions`;";
        $results = $database->select($query);

        foreach ($results as $subscription) {
            $user = $subscription['user'];
            $post = $subscription['publication'];

            switch ($subscription['type']) {
                case PublicationType::Post:
                    $post_entity = PostFactory::create_by_id($post, array('is_admin' => true));

                    $votes = PostFactory::get_votes_for_post($post_entity->get_id());
                    $upvote = null;
                    $downvote = null;
                    $upvote_count = 0;
                    $downvote_count = 0;

                    foreach ($votes as $vote) {
                        if ($vote['upvote'] == 1 && $vote['author'] != $subscription['user'] && ($upvote == null || $vote['date'] > $upvote['date'])) {
                            $upvote_count++;
                            $upvote = $vote;
                        }

                        if ($vote['upvote'] == 0 && $vote['author'] != $subscription['user'] && ($downvote == null || $vote['date'] > $downvote['date'])) {
                            $downvote_count++;
                            $downvote = $vote;
                        }
                    }

                    if ($upvote != null) {
                        $notification = SubscriptionHelper::CreateNotification($subscription['user'], $subscription['type'], array('post' => $post_entity, 'vote' => $upvote, 'count' => $upvote_count), Config::get('runtime', 'privacy'));

                        if (!is_null($notification)) {
                            SubscriptionHelper::SaveNotification($subscription['user'], $notification);
                            $notifications[] = $notification;
                        }
                    }

                    if ($downvote != null) {
                        $notification = SubscriptionHelper::CreateNotification($subscription['user'], $subscription['type'], array('post' => $post_entity, 'vote' => $downvote, 'count' => $downvote_count), Config::get('runtime', 'privacy'));

                        if (!is_null($notification)) {
                            SubscriptionHelper::SaveNotification($subscription['user'], $notification);
                            $notifications[] = $notification;
                        }
                    }

                    break;
                case PublicationType::Comment:
                    $query = "SELECT `publication_date`
                              FROM `obar_comments`
                              WHERE `post` = $post
                              AND `removed` = 0
                              AND `author` = $user
                              ORDER BY `publication_date` ASC
                              LIMIT 1";
                    $response = $database->select($query);

                    if (count($response) > 0) {
                        $date = $database->quote($response[0]['publication_date']);
                        $query = "SELECT `id`,
                                         `comment`,
                                         `author`,
                                         `image`,
                                         `publication_date`,
                                         `removed`
                                  FROM `obar_comments`
                                  WHERE `post` = $post 
                                  AND `removed` = 0
                                  AND `author` <> $user
                                  AND `publication_date` > $date
                                  ORDER BY `publication_date` DESC
                                  LIMIT 1";
                    } else {
                        $query = "SELECT `id`,
                                         `comment`,
                                         `author`,
                                         `image`,
                                         `publication_date`,
                                         `removed`
                                  FROM `obar_comments`
                                  WHERE `post` = $post 
                                  AND `removed` = 0
                                  AND `author` <> $user
                                  ORDER BY `publication_date` DESC
                                  LIMIT 1";

                    }

                    $response = $database->select($query);

                    if (count($response) > 0) {
                        $item = $response[0];
                        $comment = new Comment((int)$item['id']);
                        $comment->set_post($post);
                        $comment->set_comment($item['comment']);
                        $comment->set_author((int)$item['author']);

                        if (!is_null($item['image'])) {
                            $comment->set_image('/media/' . $item['image']);
                        }

                        $datetime = new DateTime('now');
                        $time = strtotime($item['publication_date']);
                        $time += $datetime->getOffset();
                        $value = date("Y-m-d H:i:s", $time);
                        $comment->set_publication_date($value);

                        $comment->set_removed((bool)$item['removed']);

                        $notification = SubscriptionHelper::CreateNotification($subscription['user'], $subscription['type'], $comment, Config::get('runtime', 'privacy'));

                        if (!is_null($notification)) {
                            SubscriptionHelper::SaveNotification($subscription['user'], $notification);
                            $notifications[] = $notification;
                        }
                    }

                    break;
            }
        }
        //// End Notifications

        $response = array(
            'Message' => 'Successfully synced ' . count($subscriptions) . ' subscriptions and ' . count($notifications) . ' notifications',
            'Subscriptions' => $subscriptions,
            'Notifications' => $notifications
        );
        $view->set_json_file($response);

        return $view;
    }

    // API

    public function get($params) {
        $view = new JSONView();

        if (!SessionManager::is_logged_in()) {
            throw new GenericException('Forbidden', 403);
        }

        $response['notifications'] = array();

        if (isset($params[0])) {
            $page = $params[0];
            $count = $params['count'];

            $notifications = ObarUserFactory::create_notifications($page, $count, SessionManager::get_user_id());

            if ($notifications != null) {
                foreach ($notifications as $item) {
                    $removed = false;
                    $data = json_decode($item['data']);

                    switch ($item['type']) {
                        case NotificationType::PostReply:
                        case NotificationType::PostComment:
                        case NotificationType::PostUpvote:
                        case NotificationType::PostDownvote:
                        case NotificationType::CommentUpvote:
                        case NotificationType::CommentDownvote:
                            $post = PostFactory::create_by_id($item['publication_id']);
                            $author = UserFactory::create_by_id($data->author_id);

                            if (is_null($post)) {
                                $removed = true;
                            } else {
                                $data->post_name = $post->get_name();

                                if (!Config::get('runtime', 'privacy')) {
                                    $data->author_name = $author->get_username();
                                    $data->avatar = $author->get_property('avatar');

                                    if (explode(':', $data->avatar)[0] != 'data') {
                                        $data->avatar = URLHelper::resource($data->avatar);
                                    }
                                }

                            }

                            break;
                    }

                    if (!$removed) {
                        $datetime = new DateTime('now');
                        $time = strtotime($item['date']);
                        $time += $datetime->getOffset();
                        $value = date("Y-m-d H:i:s", $time);

                        $response['notifications'][] = array(
                            'type' => $item['type'],
                            'publication_id' => $item['publication_id'],
                            'link' => $item['link'],
                            'date' => $value,
                            'seen' => $item['seen'],
                            'data' => $data
                        );
                    }
                }

                $response['count'] = ObarUserFactory::get_notification_count(SessionManager::get_user_id());
                $response['unread'] = ObarUserFactory::get_unread_notification_count(SessionManager::get_user_id());
            }
        }

        $view->set_json_file($response);
        $view->set_response_code(200);
        return $view;
    }

    public function post($params) {
        $view = new JSONView();

        if (!SessionManager::is_logged_in()) {
            throw new GenericException("You must be logged in to perform this action", 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        if ($params != null) {
            if (isset($params['user']) && isset($params['subscription'])) {
                if ($params['user'] == SessionManager::get_user_id()) {
                    SubscriptionHelper::AddSubscription($params['user'], $params['subscription']);
                }
            } elseif (isset($params['markAllRead']) && $params['markAllRead'] == true) {
                ObarUserFactory::read_all_notifications(SessionManager::get_user_id());
            } elseif (isset($params['seen'])) {
                if ($params['seen'] == "true") {
                    ObarUserFactory::read_notification($params['id'], $params['type'], SessionManager::get_user_id());
                } else {
                    ObarUserFactory::unread_notification($params['id'], $params['type'], SessionManager::get_user_id());
                }

                $view->set_json_file(array('success' => 'true'));
                $view->set_response_code(200);
            } else {
                throw new GenericException("Bad parameters", 400);
            }
        } else {
            throw new GenericException("No parameters", 400);
        }

        return $view;
    }

    public function put($params) {
        throw new GenericException("Method Not Allowed", 405);
    }

    public function delete($params) {
        throw new GenericException("Method Not Allowed", 405);
    }
}