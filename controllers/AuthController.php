<?php

namespace Apine\Controllers\User;

use Apine\Application\Config;
use Apine\Core\Collection;
use Apine\Core\Encryption;
use Apine\Core\Request;
use Apine\Modules\Gallery\MailGunHelper;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\HTMLView;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;
use Apine\Exception\GenericException;
use Apine\Session\WebSession;
use Apine\User\Factory\UserFactory;
use Apine\User\Factory\UserGroupFactory;
use Apine\User\User;
use Exception;
use Identicon\Identicon;

class AuthController extends Controller implements APIActionsInterface {
	
	// WEB
	
	public function login($params) {
        $view = new TwigView();

		if (Request::is_post()) {
			try {
				if ((isset($params['user']) && isset($params['pwd'])) && (!SessionManager::is_logged_in())) {
					if (SessionManager::login($params['user'], $params['pwd'], array("remember" => isset($params['perm'])))) {
						if (isset($params['redirect']) && !empty($params['redirect'])) {
							header("Location: " . URLHelper::path($params['redirect']));
						} else {
							header("Location: " . URLHelper::path(""));
						}
						
						die();
					}
				}
				
				$message = 'The username or the password is not valid. Please try again later.';
			} catch (Exception $e) {
				$message = 'An unknown error occured when sending data to the server. Please try again later.';
			}
            
			$view->set_param('error_code', 500);
			$view->set_param('error_message', $message);
		} else {
			if (isset(Request::get()['registration_success'])) {
				$view->set_param('error_code', 200);
				$message = 'Registration successful! You can now sign in.';
				$view->set_param('error_message', $message);
			}

			if (isset(Request::get()['request'])) {
				$view->set_param('request', Request::get()['request']);
			}
		}

        $view->set_view('login');
		return $view;
	}
    
    public function logout() {
        $view = new TwigView();

		if (SessionManager::is_logged_in()) {
			SessionManager::logout();
		}
		
		header("Location: " . URLHelper::path(""));
		return $view;
    }
    
    public function register($params) {
        $view = new TwigView();

        // Anti-spam
        $origin = explode('/', $_SERVER['HTTP_ORIGIN']);

        if (!isset($params['g-recaptcha-response']) || empty($params['g-recaptcha-response'])   // Captcha validation
            || end($origin) != $_SERVER['HTTP_HOST']                                            // Origin validation
            || (isset($params['email_confirm']) && !empty($params['email_confirm']))) {         // Fake input validation
            $message = 'An error occured. Please try again later.';
            $view->set_param('error_code', 500);
            $view->set_param('error_message', $message);
            $view->set_view('login');
        }

        if (Request::is_post() && !SessionManager::is_logged_in() && Config::get('runtime', 'registration')) {
            if (isset($params['user']) && isset($params['pwd']) && isset($params['pwd_confirm']) && isset($params['email'])) {
                try {
                    $username = htmlspecialchars_decode($params['user'], ENT_QUOTES);
                    $username = str_replace('/', '', $username);
                    $username = str_replace('\\', '', $username);
                    $username = str_replace('&', '', $username);

                    if (UserFactory::is_name_exist($username)) {
                        throw new Exception('The username is already taken by an user.');
                    }

                    if (($params['pwd'] === $params['pwd_confirm'])) {
                        $encoded_pwd = Encryption::hash_password($params['pwd']);
                    } else {
                        throw new Exception('The passwords do not match.');
                    }

                    $emailRegex = "^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$";

                    if (preg_match($emailRegex, $params['email'])) {
                        throw new Exception('The email address is not valid.');
                    }

                    if (UserFactory::is_email_exist($params['email'])) {
                        throw new Exception('The email address is not valid.');
                    }

                    $new_user = new User();
                    $new_user->set_username($username);
                    $new_user->set_password($encoded_pwd);
                    $new_user->set_type(APINE_SESSION_USER);
                    $list_group = new Collection();
                    $list_group->add_item(UserGroupFactory::create_by_id(1));
                    $new_user->set_group($list_group);
                    $new_user->set_email_address($params['email']);

                    $identicon = new Identicon();
                    $image = $identicon->getImageDataUri($params['email'], 250);

                    $new_user->set_property('avatar', $image);
                    $new_user->save();

                    $email = new HTMLView('', 'email/confirmation', 'null');
                    $email->set_param('username', $params['user']);
                    $text = <<<TEXT
Hey "{$params['user']}",

Thank you very much for registering on O-bar 18+.

We are pleased to see you in our awesome community!

Please join us at http://obar18.com

- The O-Bar 18+ administration team.
TEXT;
                    MailGunHelper::sendMail($params['user'], $params['email'], 'Thanks for registering on O-bar 18+', $text, $email->content());
                    SessionManager::login($params['email'], $params['pwd']);

                    return apine_internal_redirect('gallery' . '?registration_success');
                } catch (Exception $e) {
                    if ($e->getPrevious() == null) {
                        $message = 'An error occured. Please try again later.';
                    } else {
                        $message = $e->getMessage();
                    }

                    $view->set_param('error_code', 500);
                    $view->set_param('error_message', $message);
                    $view->set_view('login');
                }
            }
        } else {
            return apine_internal_redirect('login');
        }

        return $view;
    }
	
	// API

	public function get($params) {
		$view = new JSONView();

        if (isset($params['username']) && isset($params['password'])) {
            $auth_username = $params['username'];
            $auth_password = base64_decode($params['password']);

            try {
                if (!SessionManager::is_logged_in()) {
                    if ((new WebSession())->login($auth_username, $auth_password)) {
                        $response = true;
                        $view->set_json_file($response);
                        $view->set_response_code(200);
                    } else {
                        throw new GenericException('Unauthorized', 401);
                    }
                } else {
                    throw new GenericException('Unauthorized', 401);
                }
            } catch (Exception $e) {
                throw new GenericException($e->getMessage(), $e->getCode(), $e);
            }
        } else {
            $response = SessionManager::is_logged_in();
            $view->set_json_file($response);
            $view->set_response_code(200);
        }

		return $view;
	}
	
	public function post($params) {
		$view = new JSONView();

        if (!SessionManager::is_logged_in()) {
            throw new GenericException('Forbidden', 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        if (isset($params['password']) && isset($params['confirm']) && $params['password'] === $params['confirm']) {
            $user = SessionManager::get_user();
            $user->set_password(Encryption::hash_password($params['password']));
            $user->save();
            $view->set_json_file(true);
            $view->set_response_code(200);
        } else {
            throw new GenericException('Bad Request', 400);
        }

        return $view;
	}

	public function put($params) {
        $view = new JSONView();

        if (!SessionManager::is_logged_in()) {
            throw new GenericException('Forbidden', 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        $user = SessionManager::get_user();
        $dirty = false;

        if (isset($params['avatar'])) {
            list($type, $data) = explode(';', $params['avatar']);
            list(, $type)      = explode(':', $type);
            list(, $extension) = explode('/', $type);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $ftype = finfo_buffer($finfo, $data);

            if (isset($ftype) && $ftype == $type) {
                $filename = 'resources/public/upload/avatar_' . $user->get_id() . '.' . $extension;
                file_put_contents($filename, $data);

                $user->set_property('avatar', $filename);
                $dirty = true;
            } else {
                throw new GenericException('Bad Request', 400);
            }
        }

        if (isset($params['cover'])) {
            $user->set_property('cover', $params['cover']);
            $dirty = true;
        }

        if (isset($params['realname'])) {
            $user->set_property('realname', $params['realname']);
            $dirty = true;
        }

        if (isset($params['bio'])) {
            $user->set_property('bio', $params['bio']);
            $dirty = true;
        }

        if (isset($params['mask_mature'])) {
            $user->set_property('mask_mature', $params['mask_mature']);
            $dirty = true;
        }

        if (isset($params['email'])) {
            $user->set_email_address($params['email']);
            $dirty = true;
        }

        if ($dirty) {
            $user->save();
            $view->set_json_file(true);
            $view->set_response_code(200);
        } else {
            throw new GenericException('Bad Request', 400);
        }

        return $view;
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}