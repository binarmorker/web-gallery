<?php

namespace Apine\Controllers\User;

use Apine\Core\Encryption;
use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\EmailHelper;
use Apine\MVC\Controller;
use Apine\MVC\TwigView;
use Apine\User\Factory\PasswordTokenFactory;
use Apine\User\Factory\UserFactory;
use Apine\Utility\Routes;

class RecoveryController extends Controller {

    public function index($params) {
        if (Request::is_get()) {
            $view = new TwigView();
            $view->set_view("recovery");

            if (isset($params['email_sent'])) {
                $view->set_param('email_sent', true);
            }

            if (isset($params['password_reset'])) {
                $view->set_param('password_reset', true);
            }

            return $view;
        } else {
            $user = UserFactory::create_by_name($params['email']);

            if (is_null($user)) {
                $user = UserFactory::create_by_name($params['user']);
            }

            if (!is_null($user)) {
                EmailHelper::send_recovery_email($user);

                return Routes::internal_redirect('recovery' . '?email_sent');
            } else {
                throw new GenericException("Bad Request", 400);
            }
        }
    }

    public function reset($params) {
        if (Request::is_get()) {
            $view = new TwigView();
            $view->set_view("reset");

            PasswordTokenFactory::is_token_exist($params[0]);
            PasswordTokenFactory::is_token_valid($params[0]);
            $token = PasswordTokenFactory::create_by_token($params[0]);

            $view->set_param("user", $token->get_user());
            $view->set_param("token", $token->get_token());
            return $view;
        } else {
            PasswordTokenFactory::is_token_exist($params['token']);
            PasswordTokenFactory::is_token_valid($params['token']);
            $token = PasswordTokenFactory::create_by_token($params['token']);

            if (!is_null($token->get_user())) {
                if (isset($params['pwd']) && isset($params['pwd_confirm']) && $params['pwd'] == $params['pwd_confirm']) {
                    $user = $token->get_user();
                    $user->set_password(Encryption::hash_password($params['pwd']));
                    $user->save();

                    return Routes::internal_redirect('recovery' . '?password_reset');
                } else {
                    throw new GenericException("Bad Request", 400);
                }
            } else {
                throw new GenericException("Bad Request", 400);
            }
        }
    }

}