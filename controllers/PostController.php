<?php

namespace Apine\Controllers\User;

use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\Modules\Gallery\FileHelper;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\Modules\Gallery\SubscriptionHelper;
use Apine\Modules\Gallery\UploadFile;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;
use Apine\Application\Config;
use Apine\User\Factory\UserFactory;
use Exception;
use finfo;
use Riverline\MultiPartParser\Part;

class PostController extends Controller implements APIActionsInterface {
	
	// WEB
	
	public function index($params) {
        $view = new TwigView();
        $id = PseudoCrypt::unhash($params[0]);
        $is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
        $response = array();
        $user = SessionManager::get_user();
        $mature = false;

        if ($user) {
            $mature = $user->get_property('mature') == "true";
        }

        if (!PostFactory::is_id_exist($id, $is_admin)) {
            throw new GenericException('Not Found', 404);
        }

        $previous = PostFactory::get_previous_id($id, $mature, isset($params['user']) ? $params['user'] : null, $is_admin);
        $next = PostFactory::get_next_id($id, $mature, isset($params['user']) ? $params['user'] : null, $is_admin);

        if ($previous) {
            $response['previous'] = PseudoCrypt::hash($previous);
        } else {
            $response['previous'] = null;
        }

        if ($next) {
            $response['next'] = PseudoCrypt::hash($next);
        } else {
            $response['next'] = null;
        }

        $response['current'] = self::get_post($id, $is_admin);

        // Read notifications
        if (SessionManager::is_logged_in()) {
            foreach (SubscriptionHelper::GetAllUnreadNotificationsForPostAndUser($id, $user->get_id()) as $notification) {
                ObarUserFactory::read_notification($id, $notification['type'], $user->get_id());
            }
        }

		$view->set_view('post');
        $view->set_param('data', $response);

        if (isset($params[1])) {
            $view->set_param('comment_ref', $params[1]);
        }

        $view->set_param('is_admin', $is_admin);
		return $view;
	}

	public function getUrlItem($params) {
	    if (!Request::is_post()) {
	        throw new GenericException("Bad request", 400);
        }

	    $view = new JSONView();

	    if (isset($params['url'])) {
	        $result = array(
	            "valid" => false
            );
	        $url = parse_url($params['url']);

	        switch ($url['host']) {
                default:
                    // Check contents
                    $buffer = file_get_contents($params['url']);
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    $mimetype = $finfo->buffer($buffer);
                    $type = explode('/', $mimetype)[0];

                    if ($type == "image" || $type == "video") {
                        $base64 = 'data:' . $mimetype . ';base64,' . base64_encode($buffer);
                        $result['url'] = $base64;
                        $result['size'] = (strlen($buffer) * 8) * 0.75;
                        $result['type'] = $mimetype;
                        $urlParts = array_values(array_filter(explode('/', $url['path'])));
                        $result['name'] = end($urlParts);
                        $result['valid'] = true;
                    }

                    break;
            }

	        $view->set_json_file($result);
        }

	    return $view;
    }
	
	// API
	
	public function get($params) {
		$view = new JSONView();
		$id = PseudoCrypt::unhash($params[0]);
		$is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
		$response = array();
		$user = SessionManager::get_user();
		$mature = false;

		if ($user) {
		    $mature = $user->get_property('mature') == "true";
        }

		if (!PostFactory::is_id_exist($id, $is_admin)) {
		    throw new GenericException('Not Found', 404);
        }

        $previous = PostFactory::get_previous_id($id, $mature, isset($params['user']) ? $params['user'] : null, $is_admin);
        $next = PostFactory::get_next_id($id, $mature, isset($params['user']) ? $params['user'] : null, $is_admin);

        if ($previous) {
            $response['previous'] = PseudoCrypt::hash($previous);
        } else {
            $response['previous'] = null;
        }

        if ($next) {
            $response['next'] = PseudoCrypt::hash($next);
        } else {
            $response['next'] = null;
        }

        $response['current'] = self::get_post($id, $is_admin);
		$view->set_json_file($response);
		$view->set_response_code(200);
		return $view;
	}
	
	public function post($params) {
		$view = new JSONView();

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("You must be logged in to perform this action", 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

		$response['elements'] = array();
        $files = [];

        try {
            $document = new Part(file_get_contents('php://input'));

            if ($document != null) {
                $tempName = tempnam(sys_get_temp_dir(), 'Upl');
                file_put_contents($tempName, $document->getBody());

                $file = new UploadFile();
                $file->setType($document->getMimeType());
                $file->setName($document->getFileName());
                $file->setInternalName($tempName);
                $files[] = $file;
            }
        } catch (Exception $exception) {

        }

		if ($params != null && isset($params['uploads'])) {
            if (count($params['uploads']['file']) > 0) {
                foreach ($params['uploads']['file'] as $uploadFile) {
                    switch ($uploadFile['error']) {
                        case 1:
                        case 2:
                            throw new GenericException('Maximum file size exceeded', 500);
                            break;
                        case 3:
                        case 4:
                            throw new GenericException('No file uploaded', 400);
                            break;
                        case 6:
                        case 7:
                        case 8:
                            throw new GenericException('Failed to write file to disk', 500);
                            break;
                        default:
                            break;
                    }

                    $file = new UploadFile();
                    $file->setType($uploadFile['type']);
                    $file->setName($uploadFile['name']);
                    $file->setInternalName($uploadFile['tmp_name']);
                    $files[] = $file;
                }
            }
        } else {
            throw new GenericException("No parameters", 400);
        }

        if (count($files) > 0) {
            foreach ($files as $file) {
                $uploadedFile = FileHelper::uploadFile($file);
                $fileArr = [
                    'filename' => $uploadedFile->getName(),
                    'filetype' => $uploadedFile->getType()
                ];
                $response['elements'][] = $fileArr;
            }

            $view->set_json_file($response);
            $view->set_response_code(200);
        } else {
            throw new GenericException("No file uploaded", 400);
        }
		
		return $view;
	}
	
	public function put($params) {
		$view = new JSONView();

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("You must be logged in to perform this action", 403);
        }

		if (isset($params['id'])) {
            $is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
			$post = PostFactory::create_by_id(PseudoCrypt::unhash($params['id']), array('is_admin' => $is_admin));

			if (isset($params['name'])) {
                $name = substr($params['name'], 0, 70);
				$post->set_name($name);
			}
			
			if (isset($params['description'])) {
                $description = $params['description'] != "" ? substr($params['description'], 0, 1500) : "";
				$post->set_description($description);
			}
			
			if (isset($params['removed'])) {
			    if ($is_admin) {
                    $post->set_removed(filter_var($params['removed'], FILTER_VALIDATE_BOOLEAN));
                }
			}
			
			if (isset($params['mature'])) {
                if ($params['mature'] == true || $is_admin) {
                    $post->set_mature(filter_var($params['mature'], FILTER_VALIDATE_BOOLEAN));
                }
			}

			$post->save();
		} else {
			throw new GenericException("Not Found", 404);
		}
		
		return $view;
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}

    private function get_post($id, $is_admin) {
        $post = PostFactory::create_by_id($id, array('is_admin' => $is_admin));

        if ($post != null) {
            $username = null;
            $avatar = null;

            if (!Config::get('runtime', 'privacy')) {
                if (UserFactory::is_id_exist($post->get_author())) {
                    $user = UserFactory::create_by_id($post->get_author());
                    $username = $user->get_username();
                    $avatar = $user->get_property('avatar');

                    if (explode(':', $avatar)[0] != 'data') {
                        $avatar = URLHelper::resource($avatar);
                    }
                } else {
                    $username = "Deleted";
                }
            }

            $response['post'] = array(
                'id' => PseudoCrypt::hash($post->get_id()),
                'name' => htmlspecialchars_decode($post->get_name(), ENT_QUOTES),
                'description' => htmlspecialchars_decode($post->get_description(), ENT_QUOTES),
                'author' => $username,
                'avatar' => $avatar,
                'publication_date' => $post->get_publication_date(),
                'mature' => $post->get_mature(),
                'upvotes' => $post->get_upvotes(),
                'downvotes' => $post->get_downvotes(),
                'removed' => $post->get_removed(),
                'can_edit' => SessionManager::is_logged_in() && (SessionManager::get_user_id() == $post->get_author()),
                'vote' => null
            );

            if (SessionManager::get_instance()->is_logged_in()) {
                $user_vote = PostFactory::get_user_vote_for_post(PublicationType::Post, $id, SessionManager::get_instance()->get_user_id());

                if ($user_vote != null) {
                    $response['post']['vote'] = intval($user_vote);
                }
            }

            $response['elements'] = null;

            foreach ($post->get_elements() as $element) {
                $file = $element->get_file();

                switch (explode('/', $element->get_type())[0]) {
                    case 'video':
                        $size = array(1280, 720);
                        break;
                    case 'facebook':
                        $size = array(500, 500);
                        break;
                    case 'image':
                    default:
                        $file_parts = explode('/', $file);
                        $file_path = $_SERVER["DOCUMENT_ROOT"] . '/resources/public/upload/' . end($file_parts);
                        $size = getimagesize($file_path);
                        break;
                }

                $response['elements'][] = array(
                    'id' => $element->get_id(),
                    'file' => $file,
                    'type' => $element->get_type(),
                    'width' => $size[0],
                    'height' => $size[1]
                );
            }

            return $response;
        } else {
            throw new GenericException("This post does not exist", 404);
        }
    }
}