'use strict';

var PostViewModel = function(id, modal, json, user) {
    var self = this;
    self.id = id;
    self.post = ko.observable(null);
    self.previous = ko.observable(null);
    self.next = ko.observable(null);
    self.user = Number(user) || false;
    self.elements = ko.observableArray([]);
    self.comments = ko.observableArray([]);
    self.comment = ko.observable(null);
    self.isImageUploaded = ko.observable(false);
    self.container = ko.observable(null);
    self.report = ko.observable(null);
    self.stats = ko.observable(null);
    self.isOwner = ko.observable(false);
    self.showElements = ko.observable(false);
    self.showComments = ko.observable(false);
    self.dropzone = null;
    self.json = json;
    self.commentRef = self.json && self.json.comment_ref ? self.json.comment_ref : null;
    self.applicationName = document.title.split(' - ')[0].trim();
    self.clipboard = null;

    setLoading(true);

    if (modal) {
        var modalContainer = $('#post-modal');
        self.container(modalContainer);
        modalContainer.scrollTop(0);
    }

    self.post.subscribe(function (newValue) {
        if (modal) {
            document.title = self.applicationName + ' - ' + newValue.name();
        }
    });

    self.makeFullscreen = function(context, event) {
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
            (document.msFullscreenElement && document.msFullscreenElement !== null);

        if (isInFullScreen) {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        } else {
            var element = event.currentTarget.querySelector('img');
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullScreen) {
                element.webkitRequestFullScreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        }
    };

    self.createComment = function() {
        self.comment(new Comment());
        $('.comments').find('textarea').focus();

        self.dropzone = new Dropzone('#commentDropzone', {
            addRemoveLinks: false,
            previewsContainer: '#commentImage',
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-image"><img data-dz-thumbnail /></div></div>',
            maxFiles: 1,
            thumbnailHeight: 64,
            thumbnailWidth: 64,
            parallelUploads: 1,
            uploadMultiple: false,
            acceptedFiles: 'image/*',
            maxFilesize: 100,
            maxThumbnailFilesize: 25,
            autoProcessQueue: false,
            dictDefaultMessage: '<i class="zmdi zmdi-camera uploadIconUpload"></i><span class="close uploadIconDelete">× Remove</span>'
        });

        $(document).bind('paste', function(event){
            var items = (event.clipboardData || event.originalEvent.clipboardData).items;
            for (var index in items) {
                if (items.hasOwnProperty(index)) {
                    var item = items[index];
                    if (item.kind === 'file' && (item.type.indexOf('image', -1) >= 0) && !self.isImageUploaded()) {
                        var blob = item.getAsFile();
                        self.dropzone.addFile(blob);
                    }
                }
            }
        });

        self.dropzone.on("addedfile", function() {
            $('.dz-progress').hide();
            self.isImageUploaded(self.dropzone.files.length > 0);
            self.dropzone.removeEventListeners();
            $('.uploadIconUpload').hide();
            $('.uploadIconDelete').show();
            $('#commentDropzone').on('click', function() {
                self.dropzone.removeAllFiles();
            });
        });

        self.dropzone.on("removedfile", function() {
            self.isImageUploaded(self.dropzone.files.length > 0);
            self.dropzone.setupEventListeners();
            $('#commentDropzone').off('click');
            $('.uploadIconUpload').show();
            $('.uploadIconDelete').hide();
        });
    };

    self.cancelComment = function() {
        self.comment(null);
        self.isImageUploaded(false);
        self.dropzone.removeAllFiles();
    };

    self.saveComment = function(comment) {
        var isImage = self.isImageUploaded();
        var newComment = comment.comment();

        if (!isEmptyOrWhiteSpace(newComment) || isImage) {
            var newCommentId = comment.id();
            self.comment(null);

            if (isImage) {
                self.dropzone.on("success", function (undefined, result) {
                    self.dropzone.options.autoProcessQueue = false;
                    API.Comment.Create(
                        {
                            post: self.post().id(),
                            comment: newComment.trim(),
                            image: result.elements[0].filename
                        },
                        function () {
                            self.isImageUploaded(false);
                            self.loadComments();
                            new Success("Comment saved!");
                        },
                        function (data) {
                            new ErrorToast(data);
                        }
                    );
                });
                self.dropzone.processQueue();
            } else {
                if (comment.id() === 0) {
                    API.Comment.Create(
                        {
                            post: self.post().id(),
                            comment: newComment.trim()
                        },
                        function () {
                            self.loadComments();
                            new Success("Comment saved!");
                        },
                        function (data) {
                            new ErrorToast(data);
                        }
                    );
                } else {
                    API.Comment.Update(
                        {
                            id: newCommentId,
                            post: self.post().id(),
                            comment: newComment.trim()
                        },
                        function () {
                            self.loadComments();
                            new Success("Comment saved!");
                        },
                        function (data) {
                            new ErrorToast(data);
                        }
                    );
                }
            }
        } else {
            new ErrorToast("You cannot write an empty comment.");
        }
    };

    self.clickUpvote = function(item, type) {
        if (item.vote() === null) {
            item.vote(1);
            item.upvotes(item.upvotes() + 1);
            API.Vote.Update(
                { vote: item.vote(), type: type, post: item.id() },
                function(data) {
                },
                function(data) {
                    new ErrorToast(data);
                }
            );
        } else if (item.vote() === 0) {
            item.vote(1);
            item.downvotes(item.downvotes() - 1);
            item.upvotes(item.upvotes() + 1);
            API.Vote.Update(
                { vote: item.vote(), type: type, post: item.id() },
                function(data) {
                },
                function(data) {
                    new ErrorToast(data);
                }
            );
        } else {
            item.vote(null);
            item.upvotes(item.upvotes() - 1);
            API.Vote.Update(
                { vote: item.vote(), type: type, post: item.id() },
                function(data) {
                },
                function(data) {
                    new ErrorToast(data);
                }
            );
        }
    };

    self.clickDownvote = function(item, type) {
        if (item.vote() === null) {
            item.vote(0);
            item.downvotes(item.downvotes() + 1);
            API.Vote.Update(
                { vote: item.vote(), type: type, post: item.id() },
                function(data) {
                },
                function(data) {
                    new ErrorToast(data);
                }
            );
        } else if (item.vote() === 1) {
            item.vote(0);
            item.upvotes(item.upvotes() - 1);
            item.downvotes(item.downvotes() + 1);
            API.Vote.Update(
                { vote: item.vote(), type: type, post: item.id() },
                function(data) {
                },
                function(data) {
                    new ErrorToast(data);
                }
            );
        } else {
            item.vote(null);
            item.downvotes(item.downvotes() - 1);
            API.Vote.Update(
                { vote: item.vote(), type: type, post: item.id() },
                function(data) {
                },
                function(data) {
                    new ErrorToast(data);
                }
            );
        }
    };

    self.sendReport = function(id, type) {
        var modal = new Modal('#report-modal');
        self.report(new ReportViewModel(id, type, modal));
        self.report().load();
        modal.show();
    };

    self.showStats = function() {
        var modal = new Modal('#stats-modal');
        self.stats(new StatsViewModel(self.post().id(), modal));
        self.stats().load();
        modal.show();
    };

    self.copyUrl = function(id) {
        self.clipboard = new Clipboard('body', {
            text: function() {
                return id === 0 ? window.location.href : window.location.href + '/' + id;
            }
        });
        self.clipboard.on('success', function(e) {
            new Success("Copied!");
            e.clearSelection();
            self.clipboard.destroy();
        });
        self.clipboard.on('error', function() {
            new ErrorToast("Could not copy.");
            self.clipboard.destroy();
        });
    };

    self.showMore = function(post, event) {
        var menu = $(event.target).closest("a.more").parent().find("ul.morePopup");
        var $body = $("body");
        var menuClick = function(event) {
            if (($(event.target).closest('a').length > 0 && $(event.target).closest(".more").length === 0) || ($(event.target).closest(".more").length === 0 && $(event.target).closest(".morePopup").length === 0)) {
                menu.slideUp(500);
                $body.unbind("click", menuClick);
            }
        };

        if (menu.is(":visible")) {
            event.preventDefault();
            menu.slideUp(500);
            $body.unbind("click", menuClick);
        } else {
            event.preventDefault();
            menu.slideDown(500);
            $body.click(menuClick);
        }
    };

    self.load = function() {
        self.showElements(true);

        if (self.json) {
            setInitializing(false);
            self.setCurrentPost(self.json);
        } else {
            var args = {
                id: self.id
            };

            if (self.user) {
                args.user = self.user;
            }

            API.Post.Get(
                args,
                function (data) {
                    self.setCurrentPost(data);

                    if (modal) {
                        history.pushState({action: "showPost", data: {id: self.post().id(), name: self.post().name()}}, self.applicationName + ' - ' + self.post().name(), '/post/' + self.post().id());
                    }
                },
                function (data) {
                    if ($('#post-modal').length > 0) {
                        Modal('#post-modal').close();
                        new ErrorToast(data);
                    }
                },
                function () {
                    setInitializing(false);
                }
            );
        }
    };

    self.setCurrentPost = function(data) {
        self.post(new Post(data.current.post));

        if (modal) {
            $('#post-modal').scrollTop(0);
        } else {
            $(window).scrollTop(0);
        }

        document.title = self.applicationName + ' - ' + self.post().name();
        self.previous(data.previous);
        self.next(data.next);
        self.json = null;
        $(".description p:empty, .description span:empty").each(function (index, elem) {
            if ($(elem).siblings('a').length === 0) {
                $(elem).parent().hide();
            }
        });
        self.elements([]);
        data.current.elements.forEach(function (item) {
            self.elements.push(new Element(item));
        });
        self.showElements(data.current.elements.length <= 10);
        self.comments([]);
        self.loadComments();
    };

    self.loadPrevious = function() {
        if (self.previous()) {
            setLoading(true);

            var args = {
                id: self.previous()
            };

            if (self.user) {
                args.user = self.user;
            }

            if (modal) {
                $('#post-modal').scrollTop(0);
            } else {
                $(window).scrollTop(0);
            }

            API.Post.Get(
                args,
                function (data) {
                    self.setCurrentPost(data);
                    history.pushState({action: "showPost", data: {id: self.post().id(), name: self.post().name()}}, self.applicationName + ' - ' + self.post().name(), '/post/' + self.post().id());
                },
                function (data) {
                    if ($('#post-modal').length > 0) {
                        Modal('#post-modal').close();
                        new ErrorToast(data);
                    }
                }
            );
        }
    };

    self.loadNext = function() {
        if (self.next()) {
            setLoading(true);

            var args = {
                id: self.next()
            };

            if (self.user) {
                args.user = self.user;
            }

            if (modal) {
                $('#post-modal').scrollTop(0);
            } else {
                $(window).scrollTop(0);
            }

            API.Post.Get(
                args,
                function (data) {
                    self.setCurrentPost(data);
                    history.pushState({action: "showPost", data: {id: self.post().id(), name: self.post().name()}}, self.applicationName + ' - ' + self.post().name(), '/post/' + self.post().id());
                },
                function (data) {
                    if ($('#post-modal').length > 0) {
                        Modal('#post-modal').close();
                        new ErrorToast(data);
                    }
                }
            );
        }
    };

    if ($.ua.device.type) {
        self.hammer = new Hammer(modal ? $('#post-modal')[0] : $('body')[0]);
        var arrow = null;
        var delta = 0;

        self.hammer.on("pan", function (event) {
            if (event.offsetDirection === Hammer.DIRECTION_LEFT && event.angle < 195 && event.angle > 165) {
                $('.fixed-arrow-left').css({'left': 0});
                arrow = $('.fixed-arrow-right');
                delta = -event.deltaX / 3;
                arrow.css({'right': delta <= arrow.width() ? delta : arrow.width()});
            } else if (event.offsetDirection === Hammer.DIRECTION_RIGHT && event.angle < 15 && event.angle > -15) {
                $('.fixed-arrow-right').css({'right': 0});
                arrow = $('.fixed-arrow-left');
                delta = event.deltaX / 3;
                arrow.css({'left': delta <= arrow.width() ? delta : arrow.width()});
            } else {
                $('.fixed-arrow-left').css({'left': 0});
                $('.fixed-arrow-right').css({'right': 0});
            }
        });

        self.hammer.on("panend", function () {
            var leftArrow = $('.fixed-arrow-left');
            var rightArrow = $('.fixed-arrow-right');

            if (leftArrow.css('left').split('px')[0] >= leftArrow.width()) {
                self.loadNext();
            }

            if (rightArrow.css('right').split('px')[0] >= rightArrow.width()) {
                self.loadPrevious();
            }

            leftArrow.css({'left': 0});
            rightArrow.css({'right': 0});
        });
    } else {
        $("body").on('keydown', function(event) {
            if (!event.target.getAttribute('contenteditable') && event.target.nodeName.toLowerCase() !== 'input' && event.target.nodeName.toLowerCase() !== 'textarea') {
                switch (event.keyCode) {
                    case 37: // Left
                        self.loadNext();
                        break;
                    case 39: // Right
                        self.loadPrevious();
                        break;
                }
            }
        });
    }

    self.loadComments = function() {
        self.comment(null);
        self.showComments(true);
        API.Comment.GetForPost(
            self.post().id(),
            function(data) {
                data.comments.forEach(function(item) {
                    if (!self.comments().find(function(subItem) { return item.id === subItem.id(); })) {
                        self.comments.push(new Comment(item));
                    }
                });

                if (!self.commentRef) {
                    self.showComments(data.comments.length <= 5);
                }

                setTimeout(function() {
                    if (self.commentRef === null) {
                        $(window).trigger('scroll');
                    } else {
                        $(window).trigger('scroll');
                        setTimeout(function () {
                            var $elem = $('#comment-' + self.commentRef);
                            $elem.addClass('highlight');
                            $(window).scrollTop($elem.offset().top - ($('body > nav').height() + 8));
                            self.commentRef = null;
                        }, 1000);
                    }
                }, 0);
            },
            function(data) {
                new ErrorToast(data);
            },
            function() {
                setLoading(false);
            }
        );
    };

    self.save = function() {
        API.Post.Update(
            {
                id: self.post().id(),
                name: self.post().name(),
                description: self.post().description()
            },
            function() {
                new Success("Save successful!");
            },
            function(data) {
                new ErrorToast(data);
            }
        );
    };

    self.delete = function(item, type) {
        switch (type) {
            case 1:
                API.Post.Update(
                    {
                        id: item.id(),
                        removed: true
                    },
                    function() {
                        item.removed(true);
                        new Success("Save successful!");
                    },
                    function(data) {
                        new ErrorToast(data);
                    }
                );
                break;
            case 2:
                API.Comment.Update(
                    {
                        id: item.id(),
                        removed: true
                    },
                    function() {
                        item.removed(true);
                        new Success("Save successful!");
                    },
                    function(data) {
                        new ErrorToast(data);
                    }
                );
                break;
        }
    };

    self.undelete = function(item, type) {
        switch (type) {
            case 1:
                API.Post.Update(
                    {
                        id: item.id(),
                        removed: false
                    },
                    function() {
                        item.removed(false);
                        new Success("Save successful!");
                    },
                    function(data) {
                        new ErrorToast(data);
                    }
                );
                break;
            case 2:
                API.Comment.Update(
                    {
                        id: item.id(),
                        removed: false
                    },
                    function() {
                        item.removed(false);
                        new Success("Save successful!");
                    },
                    function(data) {
                        new ErrorToast(data);
                    }
                );
                break;
        }
    };

    self.markMature = function() {
        API.Post.SetMature(
            {
                id: self.post().id(),
                mature: true
            },
            function() {
                self.post().mature(true);
                new Success("Save successful!");
            },
            function(data) {
                new ErrorToast(data);
            }
        );
    };

    self.unmarkMature = function() {
        API.Post.SetMature(
            {
                id: self.post().id(),
                mature: false
            },
            function() {
                self.post().mature(false);
                new Success("Save successful!");
            },
            function(data) {
                new ErrorToast(data);
            }
        );
    };
};