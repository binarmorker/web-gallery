'use strict';

var LoginViewModel = function() {
    var self = this;
    self.username = ko.observable(null);
    self.password = ko.observable(null);

    self.submit = function(undefined, e) {
        e.preventDefault();
        API.Auth.Login({
            username: self.username(),
            password: Base64.encode(self.password())
        },
        function() {
            window.location.href = '/';
        },
        function() {
            new ErrorToast("Login failed");
        });
    };

    self.canSubmit = ko.computed(function() {
        return !(self.username() && self.password());
    });

    self.load = function() {
        setLoading(false);
        setInitializing(false);
    };
};