'use strict';

var GalleryViewModel = function(isMature, mask) {
    var self = this;
    self.previews = ko.observableArray([]);
    self.comments = ko.observableArray([]);
    self.post = ko.observable(null);
    self.last_date = ko.observable(null);
    self.shouldLoad = ko.observable(false);
    self.showMature = ko.observable(isMature);
    self.maskMature = ko.observable(mask);
    self.defaultTitle = document.title;
    self.applicationName = self.defaultTitle.split(' - ')[0].trim();

    self.showMature.subscribe(function(value) {
        $(window).off('scroll');
        $('.end-info').hide();
        self.last_date(null);
        self.previews([]);
        self.shouldLoad(false);
        self.load();
        API.Gallery.ShowMature({
            mature: value
        });
    });

    self.showPost = function(data) {
        self.post(new PostViewModel(data.id(), true, null, self.user_id));
        self.post().load();
        var modal = new Modal('#post-modal', null, function() {
            $("body").off("keydown");
            self.resetTitle();
            self.post(null);
            history.pushState({ action: "close" }, self.defaultTitle, '/');
        });
        modal.show();
    };

    self.resetTitle = function() {
        document.title = self.defaultTitle;
    };

    self.load = function() {
        var screenWidth = $(window).width();
        var count = 50;

        if (screenWidth < 959) {
            if (screenWidth < 549) {
                count = 15;
            } else {
                count = 30;
            }
        }

        var args = {
            count: count,
            mature: self.showMature()
        };

        if (self.last_date()) {
            args.last_date = self.last_date();
        }

        API.Gallery.Get(
            args,
            function(data) {
                if (data.previews && data.previews.length > 0) {
                    data.previews.forEach(function(item) {
                        self.previews.push(new Preview(item));
                    });

                    $('.end-info').hide();
                    $('.no-info').hide();

                    $('.grid img').imagesLoaded(function () {
                        setLoading(false);
                        setInitializing(false);
                        var $grid = $('.grid');

                        if (self.last_date()) {
                            $grid.masonry('reloadItems');
                            $grid.masonry('layout')
                        } else {
                            $grid.masonry({
                                transitionDuration: 0
                            });
                        }

                        $grid.children('a').css('opacity', '1');
                        self.last_date(data.last_date);
                        self.shouldLoad(true);

                        $(window).scroll(function() {
                            if (self.shouldLoad() && $(window).scrollTop() + $(window).height() >= getDocumentHeight() - ($(window).height() * 0.75)) {
                                self.shouldLoad(false);
                                self.load();
                            }
                        });
                    });

                    if (self.maskMature()) {
                        $('.grid .mature img').each(function (index, elem) {
                            $(elem).parent().imagesLoaded().progress(function (instance, image) {
                                $(image.img).imageBlur(50);
                            });
                        });
                    }

                    $('.grid .removed img').each(function (index, elem) {
                        $(elem).parent().imagesLoaded().progress(function (instance, image) {
                            $(image.img).imageBlur(50);
                        });
                    });
                } else {
                    setLoading(false);
                    setInitializing(false);

                    if ($('.grid img').length === 0) {
                        $('.no-info').show();
                    } else {
                        $('.end-info').show();
                    }
                }
            },
            function(data) {
                setLoading(false);
                setInitializing(false);
                $('.no-info').show();
                new ErrorToast(data);
            }
        );
    };
};