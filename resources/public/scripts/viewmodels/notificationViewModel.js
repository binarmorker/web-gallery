'use strict';

var NotificationViewModel = function() {
    var self = this;
    self.connection = null;
    self.count = ko.observable();
    self.notifications = ko.observableArray([]);
    self.timer = null;

    self.load = function() {
        var TIMER_VISIBLE = 5000;
        var TIMER_NOT_VISIBLE = 30000;

        self.timer = setInterval(self.refresh, (document.hidden) ? TIMER_NOT_VISIBLE : TIMER_VISIBLE);
        $(document).bind("visibilitychange", function() {
            clearTimeout(self.timer);
            self.timer = setInterval(self.refresh, (document.hidden) ? TIMER_NOT_VISIBLE : TIMER_VISIBLE);
        });

        initializePush();
        self.refresh();
    };

    self.refresh = function() {
        API.Notifications.Get(
            1,
            8,
            function (data) {
                self.count(data.unread);
                var oldNotifications = self.notifications();
                self.notifications([]);
                data.notifications.forEach(function (item) {
                    var message = GetNotificationMessage(item);
                    self.notifications.push(message);

                    if (oldNotifications.length > 0) {
                        var match = oldNotifications.find(function(old) {
                            return old.id() === message.id() && old.type() === message.type() && old.date() === message.date();
                        });

                        if (!match && (oldNotifications.length === 0 || message.date() > oldNotifications[0].date())) {
                            new InAppNotification(ko.toJS(message));
                        }
                    }
                });
            },
            function (data) {
                new ErrorToast(data);
            }
        );
    };

    self.markRead = function(data) {

        API.Notifications.Post(
            {
                'id': data.id(),
                'type': data.type(),
                'seen': !data.seen()
            },
            function () {
                data.seen(!data.seen());

                if (data.seen()) {
                    self.count(self.count() - 1);
                } else {
                    self.count(self.count() + 1);
                }
            },
            function () {
                new ErrorToast(data);
            }
        );
    };

    self.markAllRead = function() {
        API.Notifications.MarkAllRead();
    };
};