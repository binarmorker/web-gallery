'use strict';

var UploadViewModel = function() {
    var self = this;
    self.title = ko.observable('').extend({ charcount: 70 });
    self.description = ko.observable('').extend({ charcount: 1500 });
    self.mature = ko.observable(false);
    self.elementsLoaded = ko.observable(false);
    self.postElements = ko.observableArray([]);
    self.disabled = ko.observable(false);
    self.dropzone = null;
    self.errors = ko.observableArray([]);
    self.hasErrors = ko.observable(false);

    self.disabled.subscribe(function(newValue) {
        $(".dz-hidden-input").prop("disabled", newValue);
    });

    self.canSubmit = function() {
        return !isEmptyOrWhiteSpace(self.title()) && self.elementsLoaded();
    };

    self.submit = function() {
        $('.dz-progress').show();
        self.disabled(true);
        self.dropzone.processQueue();
    };

    self.load = function() {
        var uploadMessage = '';

        if ($.ua.engine.name !== 'EdgeHTML' && $.ua.engine.name !== 'WebKit') { // If we're not on Microsoft Edge or Google Chrome
            uploadMessage = 'Click here or drag and drop to upload';
        } else {
            if ($.ua.device.type) { // If we're on a mobile device (no drag and drop)
                uploadMessage = 'Tap here<br/> or long-press and paste to upload';
                var dropzone = $('.dropzone');
                dropzone.attr('contenteditable', true);
                dropzone.on('keyup keydown input paste', function(e) {
                    e.preventDefault();
                });
            } else { // Else, we support Ctrl+V
                uploadMessage = 'Click here, drag and drop<br/> or Ctrl+V to upload';
            }

            $(document).bind('paste', function(event){
                var items = (event.clipboardData || event.originalEvent.clipboardData).items;
                for (var index in items) {
                    if (items.hasOwnProperty(index)) {
                        var item = items[index];
                        switch (item.kind) {
                            case 'file':
                                if (item.type.indexOf('image', -1) >= 0 || item.type.indexOf('video', -1) >= 0) {
                                    var blob = item.getAsFile();
                                    self.dropzone.addFile(blob);
                                }

                                break;
                            case 'string':
                                var url = event.originalEvent.clipboardData.getData('text/plain');
                                var isValid = isValidURL(url);

                                if (isValid) {
                                    API.Post.GetUrlItem(url, function(data) {
                                        if (data.valid) {
                                            var blob = dataURLtoFile(data.url, data.name);
                                            self.dropzone.handleFiles([blob]);
                                            self.dropzone.files.push(blob);
                                            self.dropzone.createThumbnailFromUrl(blob, data.url);
                                        }
                                    });
                                }

                                break;
                        }
                    }
                }
            });
        }

        self.dropzone = new Dropzone('#dropzoneInput', {
            addRemoveLinks: true,
            parallelUploads: 1,
            uploadMultiple: true,
            acceptedFiles: 'image/*,video/*',
            maxFilesize: 100,
            maxThumbnailFilesize: 25,
            autoProcessQueue: false,
            dictDefaultMessage: uploadMessage
        });

        /* TO BE IMPLEMENTED LATER ---- SUPPORT FIREFOX AND (MAYBE) INTERNET EXPLORER

        // When a paste action is trigger, set the focus on this div and wait the data to be sent
        initPasteUploadMozilla: function () {
            $(document).on("paste", _.bind(function (a) {
                $(a.target).is("input") || this.isInView() && (Imgur.Upload.Index && this.showColorBox(), this._.el.$FFPasteBox.focus(), this.waitForPasteData(this._.el.$FFPasteBox[0]))
            }, this))
        },

        // Listen the user, and waiting that the node is created by the paste action
        waitForPasteData: function (a) {
            a.childNodes && a.childNodes.length > 0 ? this.processPaste(a) : setTimeout(this.waitForPasteData.bind(this, a), 20)
        },

        // Check data sent
        processPaste: function (a) {
            var b = a.innerHTML,
            // Check if the thing pasted is a <img tag or a png or at least base64 stuff
            c = {
                image: -1 != b.indexOf("<img") && -1 != b.indexOf("src="),
                base64: -1 != b.indexOf("base64,"),
                png: -1 != b.indexOf("iVBORw0K")
            };
            if (c.image && c.base64 && c.png) {
                var d = b.split('<img src="');
                d = d[1].split('" alt="">');
                var b = {
                    dataURL: d[0],
                    file: {
                        size: this.bytesizeBase64String(d[0].length)
                    }
                };
                this.addBase64(b)
            } else {
                var e = $(a).find("img"),
                f = null;
                if (e.length > 0 && e.attr("src").length > 0 ? f = e.attr("src") : b.length > 0 && (f = b), null !== f && f.match(this._.urlRegex)) this.queueUrl(f);
                else if (null !== f) {
                    var g = $(f).filter("a");
                    g.length && g.attr("href").match(this._.urlRegex) ? this.queueUrl(g.attr("href")) : this.showError(this._.messages.urlInvalidError)
                }
            }
            a.innerHTML = ""
        },
        */

        self.dropzone.on("addedfile", function() {
            $('.dz-progress').hide();
            self.elementsLoaded(self.dropzone.files.length > 0);
        });

        self.dropzone.on("removedfile", function() {
            self.elementsLoaded(self.dropzone.files.length > 0);
        });

        self.dropzone.on("uploadprogress", function(file, progress) {
            if (file.type.split('/')[0] === 'video') {
                var upload = $(file.previewElement).find('[data-dz-uploadprogress]');
                upload.width(progress / 1.5);
            }
        });

        self.dropzone.on("success", function(file, data) {
            ko.utils.arrayPushAll(self.postElements, data.elements);
            var upload = $(file.previewElement).find('[data-dz-uploadprogress]');
            upload.width('100%');
        });

        self.dropzone.on("processing", function() {
            self.dropzone.options.autoProcessQueue = true;
        });

        self.dropzone.on("error", function(file, reason) {
            self.dropzone.removeFile(file);
            new ErrorToast(reason);
        });
        
        self.dropzone.on("queuecomplete", function() {
            if (self.elementsLoaded()) {
                self.dropzone.options.autoProcessQueue = false;
                API.Gallery.Create(
                    {
                        elements: self.postElements(),
                        title: self.title().trim(),
                        description: self.description().trim(),
                        mature: self.mature()
                    },
                    function(data) {
                        window.location.replace(data.postUrl);
                    },
                    function(data) {
                        new ErrorToast(data);
                    }
                );
            }
        });

        setLoading(false);
        setInitializing(false);
    };
};