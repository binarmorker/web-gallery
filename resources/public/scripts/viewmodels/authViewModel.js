'use strict';

var AuthViewModel = function() {
    var self = this;
    self.login = ko.observable(new LoginViewModel());
    self.register = ko.observable(new RegisterViewModel());

    self.load = function() {
        self.login().load();
        $(window).on("recaptchaLoaded", function(event, data) {
            self.register().load(data.grecaptcha);
        });
        setLoading(false);
        setInitializing(false);
    };
};