'use strict';

var StatsViewModel = function(id, modal) {
    var self = this;
    self.id = ko.observable(id);
    self.modal = modal;
    self.votes = ko.observableArray([]);

    self.load = function() {
        API.Vote.GetForPost(self.id(), function(data) {
            data.votes.forEach(function(vote) {
                self.votes.push(new Vote(vote));
            });
        }, null, function() {
            setLoading(false);
            setInitializing(false);
        });
    }
};