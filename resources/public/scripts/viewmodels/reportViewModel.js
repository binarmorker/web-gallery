'use strict';

var ReportViewModel = function(id, type, modal) {
    var self = this;
    self.id = ko.observable(id);
    self.type = ko.observable(type);
    self.reason = ko.observable(null);
    self.modal = modal;

    self.canSubmit = ko.computed(function() {
        return !self.reason();
    });

    self.submit = function() {
        API.Rules.Report({
            item_id: self.id(),
            type: self.type(),
            description: self.reason()
        }, function() {
            new Success("Thank you for reporting this content.");
            modal.close();
        }, function(data) {
            new ErrorToast(data);
        });
    };

    self.load = function() {
        setLoading(false);
        setInitializing(false);
    }
};