'use strict';

var API = {
    Auth: {
        Login: function(data, success, error, always) {
            Request('/api/auth', 'GET', data, success, error, always);
        },
        ChangePassword: function(data, success, error, always) {
            Request('/api/auth', 'POST', data, success, error, always);
        },
        Update: function(data, success, error, always) {
            Request('/api/auth', 'PUT', data, success, error, always);
        }
    },
    Gallery: {
        Get: function(data, success, error, always) {
            Request('/api/gallery', 'GET', data, success, error, always);
        },
        GetForUser: function(data, success, error, always) {
            Request('/api/profile', 'GET', data, success, error, always);
        },
        ShowMature: function(data, success, error, always) {
            Request('/api/profile', 'POST', data, success, error, always);
        },
        Create: function(data, success, error, always) {
            Request('/api/gallery', 'POST', data, success, error, always);
        }
    },
    Post: {
        Get: function(data, success, error, always) {
            Request('/api/post/' + data.id, 'GET', data, success, error, always);
        },
        GetUrlItem: function(data, success, error, always) {
            Request('/post/getUrlItem/?url=' + data, 'POST', {}, success, error, always);
        },
        Create: function(data, success, error, always) {
            Request('/api/post', 'POST', data, success, error, always);
        },
        Update: function(data, success, error, always) {
            Request('/api/post', 'PUT', data, success, error, always);
        },
        SetMature: function(data, success, error, always) {
            Request('/api/post', 'PUT', { id: data.id, mature: data.mature }, success, error, always);
        }
    },
    Vote: {
        GetForPost: function(id, success, error, always) {
            Request('/api/vote/' + id, 'GET', {}, success, error, always);
        },
        Update: function(data, success, error, always) {
            Request('/api/vote', 'POST', data, success, error, always);
        }
    },
    Comment: {
        GetForPost: function(id, success, error, always) {
            Request('/api/comments/' + id, 'GET', {}, success, error, always);
        },
        Create: function(data, success, error, always) {
            Request('/api/comments', 'POST', data, success, error, always);
        },
        Update: function(data, success, error, always) {
            Request('/api/comments', 'PUT', data, success, error, always);
        }
    },
    Rules: {
        Get: function(success, error, always) {
            Request('/api/rules', 'GET', {}, success, error, always);
        },
        Report: function(data, success, error, always) {
            Request('/api/rules', 'POST', data, success, error, always);
        }
    },
    Members: {
        Get: function(page, success, error, always) {
            Request('/api/members/' + page, 'GET', {}, success, error, always);
        }
    },
    Notifications: {
        Get: function(page, count, success, error, always) {
            Request('/api/notifications/' + page, 'GET', { count: count }, success, error, always);
        },
        Post: function(data, success, error, always) {
            Request('/api/notifications', 'POST', data, success, error, always);
        },
        MarkAllRead: function(success, error, always) {
            Request('/api/notifications', 'POST', { markAllRead: true }, success, error, always);
        },
        Subscribe: function(userid, subscription, success, error, always) {
            Request('/api/notifications', 'POST', { user: userid, subscription: subscription }, success, error, always);
        }
    }
};