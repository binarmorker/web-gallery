'use strict';

var Element = function(data) {
    var self = this;
    self.id = ko.observable(0);
    self.post = ko.observable(null);
    self.file = ko.observable('');
    self.width = ko.observable(0);
    self.height = ko.observable(0);
    self.type = ko.observable('');
    self.removed = ko.observable(0);
    self.reported = ko.observable(0);
    
    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};