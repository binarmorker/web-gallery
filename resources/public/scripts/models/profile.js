'use strict';

var Profile = function(data) {
    var self = this;
    self.username = ko.observable('');
    self.avatar = ko.observable('');
    self.bio = ko.observable('').extend({ charcount: 150 });
    self.realname = ko.observable('');
    self.cover = ko.observable('');
    self.email = ko.observable('');
    self.is_self = ko.observable(false);
    self.mask_mature = ko.observable(true);
    self.posts = ko.observable(0);
    self.registration_date = ko.observable('');
    self.groups = ko.observable('');
    self.reputation = ko.observable(0);
    self.temper = ko.observable(0);

    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};