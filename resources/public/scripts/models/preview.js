'use strict';

var Preview = function(data) {
    var self = this;
    self.id = ko.observable(0);
    self.image = ko.observable('');
    self.name = ko.observable('');
    self.mature = ko.observable(false);
    self.removed = ko.observable(false);

    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};