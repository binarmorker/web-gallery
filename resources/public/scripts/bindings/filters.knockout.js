'use strict';

ko.components.register('filters', {
    viewModel: function (params) {
        var self = this;

        if (typeof params.options === "undefined" ||
            typeof params.value === "undefined") {
            return;
        }

        self.id = uuidv4();
        self.active = ko.observable(false);
        self.active.subscribe(function(value) {
            var callback = function (e) {
                if ($(e.target).closest('.filtersDropdown').attr('id') !== self.id ||
                    ($(e.target).is('.option') &&
                    !$(e.target).closest('.filtersDropdown').hasClass('multiple'))) {
                    self.active(false);
                }
            };

            if (value) {
                $('body').on('click', callback);
            } else {
                $('body').off('click', callback);
            }
        });
        self.options = params.options;
        self.textField = params.textField || null;
        self.valueField = params.valueField || null;
        self.isMultiple = params.multiple;
        self.value = params.value || ko.observable();
        self.caption = params.caption || null;

        var defValue = ko.unwrap(self.options).filter(function(e) { return e[self.valueField] === ko.unwrap(params.default) })[0];
        self.defaultValue = (typeof params.default !== "undefined" ? defValue : false) || ko.unwrap(self.options)[0];
        self.selection = ko.observable(self.caption || self.defaultValue[self.textField]);

        self.selectValue = function(data, event) {
            if (self.isMultiple) {
                var values = ko.unwrap(self.value) || [];
                var newValues = [];
                var text = "";

                ko.unwrap(self.options).forEach(function(e) {
                    if (e === data) {
                        if (values.filter(function (i) { return i === e[self.valueField] }).length > 0) {
                            $(event.target).removeClass('active');
                        } else {
                            $(event.target).addClass('active');
                            newValues.push(e);
                        }
                    } else if (values.filter(function (i) { return i === e[self.valueField] }).length > 0) {
                        newValues.push(e);
                    }
                });

                newValues.forEach(function(e) {
                    if (text === "") {
                        text = e[self.textField];
                    } else {
                        text += ", " + e[self.textField];
                    }
                });

                if (newValues.length === 0) {
                    text = self.caption;
                }

                self.selection(text);
                self.value(newValues.map(function(e) { return e[self.valueField] }));
            } else {
                $(event.target).parent().find('.option').removeClass('active');
                $(event.target).addClass('active');
                self.selection(data[self.textField]);
                self.value(data[self.valueField]);
            }
        };
    },
    template:
        '<div data-bind="attr: { \'id\': id }, click: function() { active(true) }, css: { \'active\': active, \'multiple\': isMultiple }" class="filtersDropdown">\
            <span class="selection" data-bind="text: selection"></span>\
            <ul class="options" data-bind="foreach: options">\
                <li class="option" data-bind="click: $parent.selectValue, text: $data[$parent.textField]"></li>\
            </ul>\
        </div>'
});