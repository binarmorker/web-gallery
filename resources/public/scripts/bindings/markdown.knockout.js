'use strict';

ko.bindingHandlers.markdown = {
    update: function(element, valueAccessor) {
        if (valueAccessor()) {
            var value = ko.utils.unwrapObservable(valueAccessor());

            if (value) {
                $(element).html(marked(value));
            }
        }
    }
};