'use strict';

ko.components.register('inline', {
    viewModel: function (params) {
        var self = this;

        if (typeof params.value === "undefined" || typeof params.save === "undefined") {
            return;
        }

        self.value = params.value;
        self.previousValue = self.value();
        self.editing = ko.observable(false);

        self.edit = function (vm, e) {
            e.preventDefault();
            self.editing(true);
            autosize.update($('textarea'));
        }.bind(self);

        self.save = function (vm, e) {
            e.preventDefault();
            self.editing(false);
            self.previousValue = self.value();
            params.save(self.value());
        }.bind(self);

        self.onKeyPress = function(vm, e) {
            var keyCode = (e.which ? e.which : e.keyCode);

            if (keyCode === 13) {
                this.save(vm, e);
                return false;
            }

            return true;
        }.bind(this);

        self.clear = function (vm, e) {
            e.preventDefault();
            self.value(self.previousValue);
            self.editing(false);
        }.bind(self);
    },
    template:
        '<span data-bind="click: edit, text: value, visible: !editing()"></span>\
        <textarea data-bind="autosize: value, textInput: value, visible: editing, css: { charMaxError: value.css }"></textarea>\
        <a class="inline-icon" href="#!" data-bind="click: edit, visible: !editing()"><i class="zmdi zmdi-edit"></i></a>\
        <a class="inline-icon" href="#!" data-bind="click: save, visible: editing" style="display:none"><i class="zmdi zmdi-check"></i></a>\
        <a class="inline-icon" href="#!" data-bind="click: clear, visible: editing" style="display:none"><i class="zmdi zmdi-close"></i></a>'
});