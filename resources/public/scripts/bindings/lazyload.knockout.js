'use strict';

ko.bindingHandlers.lazyload = {
    init: function(element, valueAccessor) {
        if (valueAccessor()) {
            var params = ko.utils.unwrapObservable(valueAccessor());
            var src = null;
            var options = {
                effect : "fadeIn",
                threshold : 200
            };

            if (typeof params() === 'object') {
                if (params().src) {
                    src = params().src;
                }
                
                if (params().container) {
                    options.container = params().container;
                }
            } else {
                src = params;
            }

            if (src !== null) {
                $(element).attr('data-original', src);
                $(element).lazyload(options);
            }
        }
    }
};