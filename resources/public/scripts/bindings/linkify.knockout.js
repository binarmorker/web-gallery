'use strict';

ko.bindingHandlers.linkify = {
    update: function(element, valueAccessor) {
        var value = "";

        if (typeof valueAccessor === "function") {
            value = ko.unwrap(valueAccessor());
        } else {
            value = ko.unwrap(valueAccessor);
        }

        var options = {
            format: function(value) {
                if (value.length > 30) {
                    value = value.substr(0, 20) + '…' + value.substr(value.length - 8, value.length);
                }

                return value.replace(/https?:\/\/(www\.)*/, '');
            }
        };

        ko.utils.setHtml(element, linkifyStr(value, options));
    }
};