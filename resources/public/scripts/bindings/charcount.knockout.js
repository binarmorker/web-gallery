'use strict';

ko.extenders.charcount = function (target, maxLength) {
    var result = ko.pureComputed({
        read: target,
        write: function (val) {
            if (maxLength > 0) {
                if (val.length > maxLength) {
                    var limitedVal = val.substring(0, maxLength);
                    if (target() === limitedVal) {
                        target.notifySubscribers();
                    }
                    else {
                        target(limitedVal);
                    }
                    result.css("errorborder");
                    setTimeout(function () { result.css(""); }, 500);
                }
                else { target(val); }
            }
            else { target(val); }
            result.charcount(maxLength - val.length > 0 ? maxLength - val.length : 0);
        }
    }).extend({ notify: 'always' });
    result.css = ko.observable();
    result.charcount = ko.observable();
    result(target());
    return result;
};