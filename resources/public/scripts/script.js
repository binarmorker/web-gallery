'use strict';

$('body').on('focus', '[contenteditable]', function() { // Content editable before change
    var $this = $(this);
    $this.data('before', $this.html());
    return $this;
}).on('blur keyup paste input', '[contenteditable]', function() { // Content editable change
    var $this = $(this);
    if ($this.data('before') !== $this.html()) {
        $this.data('before', $this.html());
        $this.trigger('change');
    }
    return $this;
}).on('click', '.tabSelector a', function(e) { // Tab functionality
	e.preventDefault();
	var target = $(e.currentTarget);
	var tabGroup = target.closest('.tabSelector');
	var tabContent = tabGroup.next('.tabs');
	var index = target.parentsUntil('.tabSelector').index();
    tabGroup.children().removeClass('selected');
	target.parentsUntil('.tabSelector').addClass('selected');
	tabContent.children().removeClass('selected');
	tabContent.children().eq(index).addClass('selected');
});

(function() {
    var hidden = "hidden";

    // Standards:
    if (hidden in document)
        document.addEventListener("visibilitychange", onchange);
    else if ((hidden = "mozHidden") in document)
        document.addEventListener("mozvisibilitychange", onchange);
    else if ((hidden = "webkitHidden") in document)
        document.addEventListener("webkitvisibilitychange", onchange);
    else if ((hidden = "msHidden") in document)
        document.addEventListener("msvisibilitychange", onchange);
    // IE 9 and lower:
    else if ("onfocusin" in document)
        document.onfocusin = document.onfocusout = onchange;
    // All others:
    else
        window.onpageshow = window.onpagehide
            = window.onfocus = window.onblur = onchange;

    function onchange (evt) {
        var v = "visible", h = "hidden",
            evtMap = {
                focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
            };

        evt = evt || window.event;
        if (evt.type in evtMap)
            document.body.className = evtMap[evt.type];
        else
            document.body.className = this[hidden] ? "hidden" : "visible";
    }

    // set the initial state (but only if browser supports the Page Visibility API)
    if( document[hidden] !== undefined )
        onchange({type: document[hidden] ? "blur" : "focus"});
})();

function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(',');
    var mime = arr[0].match(/:(.*?);/)[1];
    var bstr = atob(arr[1]);
    var n = bstr.length;
    var u8arr = new Uint8Array(n);

    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, {type: mime});
}

var Base64 = {
	_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	encode: function(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		input = Base64._utf8_encode(input);

		while (i < input.length) {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
		}

		return output;
	},
	decode: function(input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
			output = output + String.fromCharCode(chr1);

			if (enc3 !== 64) {
				output = output + String.fromCharCode(chr2);
			}

			if (enc4 !== 64) {
				output = output + String.fromCharCode(chr3);
			}
		}

		output = Base64._utf8_decode(output);
		return output;
	},
	_utf8_encode: function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
		}

		return utftext;
	},
	_utf8_decode: function(utftext) {
		var string = "";
		var i = 0;
		var c = 0;
		var c2 = 0;
		var c3 = 0;

		while (i < utftext.length) {
			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}

		return string;
	}
};

var ErrorToast = function(content) {
	var message = "An unknown error occured.";

	if (content !== undefined) {
        if (content.message !== undefined && typeof content.message === 'string' && content.message !== '') {
            message = content.message;
        } else if (content.responseJSON !== undefined && content.responseJSON.message !== undefined && typeof content.responseJSON.message === 'string' && content.responseJSON.message !== '') {
            message = content.responseJSON.message;
        } else if (typeof content === 'string' && content !== '') {
        	message = content;
		}
    }

	new PNotify({
		title: 'Error',
		text: message,
		icon: false,
        type: 'error',
		confirm: {
			confirm: true,
			buttons: [{
				text: 'Dismiss',
				click: function(notice) {
					notice.remove();
				}
			},
			null]
		},
		buttons: {
			closer: false,
			sticker: false
		}
	});
};

var Success = function(message) {
    new PNotify({
        title: 'Success',
        text: message,
        icon: false,
		type: 'success',
        confirm: {
            confirm: true,
            buttons: [{
                text: 'Dismiss',
                click: function(notice) {
                    notice.remove();
                }
            },
            null]
        },
        buttons: {
            closer: false,
            sticker: false
        }
    });
};

var InAppNotification = function(data) {
	var avatar = data.icon ? ('<img src="' + data.icon + '" class="avatar">') : '';

    new PNotify({
    	title: avatar + data.message,
        text: data.date,
        icon: false,
        type: 'info',
		hide: false,
        confirm: {
            confirm: true,
            buttons: [{
                text: 'Go',
                click: function(notice) {
                    notice.remove();
                	window.location = data.link;
                }
            }, {
                text: 'Dismiss',
                click: function(notice) {
                    notice.remove();
                }
            }]
        },
        buttons: {
            closer: true,
            sticker: false
        }
    });
};

function GetNotificationMessage(message) {
    if (typeof message.data === 'string') {
        message = JSON.parse(message.data);
    }

    var notificationMessage = '';
    var avatar = null;

    switch (parseInt(message.type)) {
        case 1: // Post reply
            if (message.data.author_name) {
                notificationMessage = message.data.author_name;
                avatar = message.data.avatar;
            } else {
                notificationMessage = 'A user';
            }

            notificationMessage += ' commented on your post: "' + message.data.post_name + '".';

            break;
        case 2: // Post comment
            if (message.data.author_name) {
                notificationMessage = message.data.author_name;
                avatar = message.data.avatar;
            } else {
                notificationMessage = 'A user';
            }

            notificationMessage += ' commented on a post you are following: "' + message.data.post_name + '".';

            break;
        case 3: // Post upvote
            if (message.data.author_name) {
        		if (message.data.votes_count > 1) {
                    notificationMessage = message.data.author_name + ' and ' + (message.data.votes_count - 1) + ' other user' + (message.data.votes_count - 1 < 2 ? '' : 's');
				} else {
                    notificationMessage = message.data.author_name;
				}

                avatar = message.data.avatar;
            } else {
                notificationMessage = message.data.votes_count + ' users';
            }

            notificationMessage += ' liked your post: "' + message.data.post_name + '".';

            break;
        case 4: // Post downvote
            if (message.data.author_name) {
                if (message.data.votes_count > 1) {
                    notificationMessage = message.data.author_name + ' and ' + (message.data.votes_count - 1) + ' other users';
                } else {
                    notificationMessage = message.data.author_name;
                }

                avatar = message.data.avatar;
            } else {
                notificationMessage = message.data.votes_count + ' users';
            }

            notificationMessage += ' hated your post: "' + message.data.post_name + '".';

            break;
        case 5: // Comment upvote
            if (message.data.author_name) {
                if (message.data.votes_count > 1) {
                    notificationMessage = message.data.author_name + ' and ' + (message.data.votes_count - 1) + ' other user' + (message.data.votes_count - 1 < 2 ? '' : 's');
                } else {
                    notificationMessage = message.data.author_name;
                }

                avatar = message.data.avatar;
            } else {
                notificationMessage = message.data.votes_count + ' users';
            }

            notificationMessage += ' liked your comment on post: "' + message.data.post_name + '".';

            break;
        case 6: // Comment downvote
            if (message.data.author_name) {
                if (message.data.votes_count > 1) {
                    notificationMessage = message.data.author_name + ' and ' + (message.data.votes_count - 1) + ' other users';
                } else {
                    notificationMessage = message.data.author_name;
                }

                avatar = message.data.avatar;
            } else {
                notificationMessage = message.data.votes_count + ' users';
            }

            notificationMessage += ' hated your comment on post: "' + message.data.post_name + '".';

            break;
    }

    return ko.mapping.fromJS({
        id: message.publication_id,
        icon: avatar,
        date: message.date,
        message: notificationMessage,
        link: message.link,
        type: parseInt(message.type),
        seen: parseInt(message.seen)
    });
}

function logErrors(data) {
    data.ua = UAParser();
    var request = new XMLHttpRequest();
    request.open('POST', '/logger', true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    request.send(JSON.stringify(data));
}

if (window && !window.onerror) {
    window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
        logErrors({
            "message": "Exception: " + errorMsg,
            "url": url,
            "line number": lineNumber,
            "column": column,
            "data": errorObj
        });

        return false;
    }
}


if (typeof window !== 'undefined' && !window.onunhandledrejection) {
    window.onunhandledrejection = function (event) {
        logErrors({
            "message": "Promise: " + event.reason ? event.reason.message : null,
            "data": event
        });

        return false;
    };
}

function initializePush() {
    if (window.Notification && window.firebase) {
        var messaging = firebase.messaging();

        messaging.requestPermission().then(function () {
            messaging.getToken().then(function (currentToken) {
                API.Notifications.Subscribe(user, currentToken);
            });
        });

        messaging.onTokenRefresh(function () {
            messaging.getToken().then(function (refreshedToken) {
                API.Notifications.Subscribe(user, refreshedToken);
            });
        });
    }
}

function getDocumentHeight() {
    return Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

window.isValidURL = (function() {// wrapped in self calling function to prevent global pollution

    //URL pattern based on rfc1738 and rfc3986
    var rg_pctEncoded = "%[0-9a-fA-F]{2}";
    var rg_protocol = "(http|https):\\/\\/";

    var rg_userinfo = "([a-zA-Z0-9$\\-_.+!*'(),;:&=]|" + rg_pctEncoded + ")+" + "@";

    var rg_decOctet = "(25[0-5]|2[0-4][0-9]|[0-1][0-9][0-9]|[1-9][0-9]|[0-9])"; // 0-255
    var rg_ipv4address = "(" + rg_decOctet + "(\\." + rg_decOctet + "){3}" + ")";
    var rg_hostname = "([a-zA-Z0-9\\-\\u00C0-\\u017F]+\\.)+([a-zA-Z]{2,})";
    var rg_port = "[0-9]+";

    var rg_hostport = "(" + rg_ipv4address + "|localhost|" + rg_hostname + ")(:" + rg_port + ")?";

    // chars sets
    // safe           = "$" | "-" | "_" | "." | "+"
    // extra          = "!" | "*" | "'" | "(" | ")" | ","
    // hsegment       = *[ alpha | digit | safe | extra | ";" | ":" | "@" | "&" | "=" | escape ]
    var rg_pchar = "a-zA-Z0-9$\\-_.+!*'(),;:@&=";
    var rg_segment = "([" + rg_pchar + "]|" + rg_pctEncoded + ")*";

    var rg_path = rg_segment + "(\\/" + rg_segment + ")*";
    var rg_query = "\\?" + "([" + rg_pchar + "/?]|" + rg_pctEncoded + ")*";
    var rg_fragment = "\\#" + "([" + rg_pchar + "/?]|" + rg_pctEncoded + ")*";

    var rgHttpUrl = new RegExp(
        "^"
        + rg_protocol
        + "(" + rg_userinfo + ")?"
        + rg_hostport
        + "(\\/"
        + "(" + rg_path + ")?"
        + "(" + rg_query + ")?"
        + "(" + rg_fragment + ")?"
        + ")?"
        + "$"
    );

    // export public function
    return function (url) {
        return rgHttpUrl.test(url);
    };
})();

function isEmptyOrWhiteSpace(value) {
    if (typeof(value) === "string") {
        var cleanedValue = value.trim();

        return cleanedValue.length <= 0;
    }

    return true;
}

function setInitializing(value) {
	vm.initializing(value);
}

function setLoading() {
    vm.loading(false);
}

$(window).on("popstate", function(event) {
	event.preventDefault();

	if ($("body").hasClass("modal-open")) {
        $(".modal.open").each(function() {
            var modal = $(this).data("modal");
            modal.close();
        });
	} else {
		window.location.reload();
	}
});

function loadCaptcha() {
	$(window).trigger("recaptchaLoaded", {
		grecaptcha: grecaptcha
	});
}

var Modal = function(selector, onOpen, onClose) {
	var self = this;
	self.selector = selector;
	self.onOpen = onOpen;
	self.onClose = onClose;

	self.show = function() {
        $(self.selector).data("modal", self);
		$(self.selector).show();
		$(self.selector).focus();
		$("body").addClass("modal-open");
		$(self.selector).addClass("open");
		$(self.selector).on("click", function(event) {
			if ($(event.target).hasClass("modal") || $(event.target).attr("data-close") !== undefined) {
				self.close();
			}
		});
		$(self.selector).on("keydown", function(event) {
			if (event.keyCode === 27 /*ESC*/) {
				self.close();
			}
		});

		if (self.onOpen) {
			self.onOpen();
		}
	};

	self.close = function() {
        $(self.selector).data("modal", null);
        $(self.selector).hide();
		$(self.selector).off();
		$(self.selector).removeClass("open");
		$("body").removeClass("modal-open");
		$(".modal.open").last().focus();

		if (self.onClose) {
			self.onClose();
		}
	};

	return self;
};