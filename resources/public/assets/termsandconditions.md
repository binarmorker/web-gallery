# Terms of Service ("Terms")

Last updated: April 23, 2017

Please read these Terms of Service ("Terms", "Terms of Service") carefully before using the http://obar18.com website ("O-Bar", the "Website" or the "Service") operated by O-Bar ("us", "we", or "our").

Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.

By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.

When using the Service, you shall be subject to any posted rules, community guidelines, or policies. Such rules, guidelines, and policies are hereby incorporated by reference into these Terms of Service.

**Ability to Accept Terms of Service**

You affirm that you are at least 18 years of age or the age of majority in the jurisdiction you are accessing the Website from, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms of Service, and to abide by and comply with these Terms of Service. If you are under 18 or the applicable age of majority, you are not permitted to submit personal information to us or use the Website. You also represent that the jurisdiction from which you access the Website does not prohibit the receiving or viewing of sexually explicit content.

**About Our Website**

The Website allows for uploading, sharing and general viewing of texts, messages, files, data, information, images, photos, videos, recordings, materials, code or content of any kind and other materials posted/uploaded by registered users. In addition, the Website contains various types of adult-oriented content by registered users who desire to share and view visual depictions of adult-oriented content, including sexually explicit images.

You understand and acknowledge that when using the Website, you will be exposed to content from a variety of sources, and that the Website is not responsible for the accuracy, usefulness, safety, or intellectual property rights of or relating to such content. You further understand and acknowledge that you may be exposed to content that is inaccurate, offensive, indecent, or objectionable, and you agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against the Website with respect thereto, and agree to indemnify and hold the Website, its site operator, its parent corporation, their respective affiliates, licensors, service providers, officers, directors, employees, agents, successors and assigns, harmless to the fullest extent allowed by law regarding all matters related to your use of the Website.

In the event you use our Website over mobile devices, you hereby acknowledge that your carrier’s normal rates and fees, such as excess broadband fees will still apply.

We have the right to disable any user name, password or other identifier, whether chosen by you or provided by us, at any time in our sole discretion for any or no reason, including if, in our opinion, you have violated any provision of these Terms of Service.

**Accounts**

When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.

You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.

You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.

**Links To Other Web Sites**

Our Service may contain links to third-party web sites or services that are not owned or controlled by O-Bar.

O-Bar has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that O-Bar shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.

We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.

**Content Posted by Users**

The Website may contain message boards, chat rooms, personal web pages or profiles, playlists, forums, bulletin boards and other interactive features (collectively, "Interactive Services") that allow users to upload, post, create, submit, publish, make available, send, share, communicate, display or transmit to other users or other persons (collectively, "post") content, data, information, videos, images, recordings, materials, code or content of any kind (collectively, "Content") on or through the Website.

As a Website account holder you may submit Content to the Website and other Websites linked to the Website including images, videos and user comments. You understand that the Website does not guarantee any confidentiality with respect to any Content you submit. 

You shall be solely responsible for your own Content and the consequences of posting, uploading, publishing transmitting or otherwise making available your Content on the Website. You understand and acknowledge that you are responsible for any Content you submit or contribute, and you, not us, have full responsibility for such Content, including its legality, reliability, accuracy and appropriateness. We are not responsible, or liable to any third party, for the content or accuracy of any Content posted by you or any other user of the Website. We do not control Content you submit or contribute and we do not make any guarantee whatsoever related to Content submitted or contributed by users. Although we sometimes review Content submitted or contributed by users, we are not obligated to do so. Under no circumstances will we be liable or responsible in any way for any claim related to Content submitted or contributed by users.

You affirm, represent, and warrant that you own or have the necessary licenses, rights, consents, and permissions to publish Content you submit; and you license to the Website all patent, trademark, trade secret, copyright or other proprietary rights in and to such Content for publication on the Website pursuant to these Terms of Service.

You further agree that Content you submit to the Website will not contain third party copyrighted material, or material that is subject to other third party proprietary rights, unless you have permission from the rightful owner of the material or you are otherwise legally entitled to post the material and to grant to the Website all of the license rights granted herein.

For clarity, you retain all of your ownership rights in your Content. However, by submitting Content to the Website, you hereby grant the Website a worldwide, irrevocable, perpetual, non-exclusive, royalty-free, sublicenseable and transferable license to use, exploit, reproduce, distribute, prepare derivative works of, display, communicate, and perform the Content in connection with the Website’s (and its successors’ and affiliates’) business, including without limitation for promoting and redistributing part or all of the Website (and derivative works thereof) in any media formats and through any media channels. You also waive to the full extent permitted by law any and all claims against us related to moral rights in the Content. In no circumstances will we be liable to you for any exploitation of any Content that you post. You also hereby grant each user of the Website a non-exclusive, royalty free license to access your Content through the Website, and to use, reproduce, distribute, display, communicate and perform such Content as permitted through the functionality of the Website and under these Terms of Service. The above licenses granted by you in images and video content you submit to the Website terminate within a commercially reasonable time after you remove or delete your images and videos from the Website. You understand and agree, however, that the Website may retain, but not display, distribute, or perform, server copies of your images and videos that have been removed or deleted. The above licenses granted by you in user comments you submit are perpetual and irrevocable.

The Website does not endorse any Content submitted to it by any user or other licensor, or any opinion, recommendation, or advice expressed therein, and the Website expressly disclaims any and all liability in connection with Content. The Website does not permit copyright infringing activities and infringement of intellectual property rights on the Website, and the Website will remove all Content if properly notified that such Content infringes on another’s intellectual property rights. The Website reserves the right to remove Content without prior notice.

All Content you submit must comply with the Content standards set out in these Terms of Service.

If any of the Content that you post to or through the Website contains ideas, suggestions, documents, and/or proposals to us, we will have no obligation of confidentiality, express or implied, with respect to such Content, and we shall be entitled to use, exploit or disclose (or choose not to use or disclose) such Content at our sole discretion without any obligation to you whatsoever (i.e., you will not be entitled to any compensation or reimbursement of any kind from us under any circumstances).

In the process of posting Content to the Website, you may be asked to provide some personally identifying information, such as your name, address, e-mail address, a password, and other documentation. You may also be asked to provide such information in order to use certain features of the Website.

We will keep a record of the information you provide, including your personally identifiable information. That information may be linked in our records to other information you provide, including Content. We will not provide your name or other personally identifying information to our advertisers or business partners without your permission. Please note that some of the information you provide in registering for and using the Website, including the name used in registering for and using the Website or other personally identifying information, may be displayed to other members of the Website, and may become public. In addition, we may disclose the personally identifying information and documentation you provide in some limited circumstances.

**Prohibited Uses**

You agree that you will only use the Website and our services for the lawful purposes expressly permitted and contemplated by these Terms of Service. You may not use the Website and our services for any other purposes, including commercial purposes, without our express written consent.

You agree that you will view the Website and its content unaltered and unmodified. You acknowledge and understand that you are prohibited from modifying the Website or eliminating any of the content of the Website, including ads. By using the Website you expressly agree to accept advertising served on and through the Website and to refrain from using ad blocking software or to disable ad blocking software before visiting the Website.

You agree that you will not use or attempt to use any method, device, software or routine to harm others or interfere with the functioning of the Website or use and/or monitor any information in or related to the Website for any unauthorized purpose. Specifically, you agree not to use the Website to:
- violate any law (including without limitation laws related to torts, contracts, patents, trademarks, trade secrets, copyrights, defamation, obscenity, pornography, rights of publicity or other rights) or encourage or provide instructions to another to do so; 
- act in a manner that negatively affects other users’ ability to use the Website, including without limitation by engaging in conduct that is harmful, threatening, abusive, inflammatory, intimidating, violent or encouraging of violence to people or animals, harassing, stalking, invasive of another’s privacy, or racially, ethnically, or otherwise objectionable;
- post any Content that depicts any person under 18 years of age (or older in any other location in which 18 is not the minimum age of majority);
- post any Content for which you have not maintained written documentation sufficient to confirm that all subjects of your posts are, in fact, over 18 years of age (or older in any other location in which 18 is not the minimum age of majority); 
- post any Content depicting child pornography, rape, snuff, torture, death, violence, or incest, racial slurs or hate speech, (either aurally or via the written word);
- post any Content that contains falsehoods or misrepresentations that could damage the Website or any third party;
- post any Content that is obscene, illegal, unlawful, defamatory, libelous, harassing, hateful, racially or ethnically offensive, or encourages conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate;
- post any Content containing unsolicited or unauthorized advertising, promotional materials, spam, junk mail, chain letters, pyramid schemes or any other form of unauthorized solicitation; 
- post any Content containing sweepstakes, contests, or lotteries, or otherwise related to gambling; 
- post any Content containing copyrighted materials, or materials protected by other intellectual property laws, that you do not own or for which you have not obtained all necessary written permissions and releases;
- post any Content which impersonates another person or falsely state or otherwise misrepresent your affiliation with a person;
- deploy programs, software or applications designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment, including by engaging in any denial of service attack or similar conduct; 
- deploy or use programs, software or applications designed to harm, interfere with the operation of, or access in an unauthorized manner, services, networks, servers, or other infrastructure;
- exceed your authorized access to any portion of the Website; 
- remove, delete, alter, circumvent, avoid or bypass any digital rights management technology, encryption or security tools used anywhere on the Website or in connection with our services; 
- collect or store personal data about anyone; 
- alter or modify without permission any part of the Website or its content, including ads; 
- obtain or attempt to access or otherwise obtain any Content or information through any means not intentionally made available or provided for through the Website; 
- exploit errors in design, features which are not documented and/or bugs to gain access that would otherwise not be available.

Additionally, you agree not to:
- use the Website in any manner that could disable, overburden, damage, or impair the site or interfere with any other party’s use of the Website, including their ability to engage in real time activities through the Website;
- use any robot, spider or other automatic device, process or means to access the Website for any purpose, including monitoring or copying any of the material on the Website without our prior written consent;
- use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent;
- use any information obtained from or through the Website to block or interfere with the display of any advertising on the Website, or for the purpose of implementing, modifying or updating any software or filter lists that block or interfere with the display of any advertising on the Website;
- use any device, bots, scripts, software or routine that interferes with the proper working of the Website or that shortcut or alter Website functions to run or appear in ways that are not intended by Website design;
- introduce or upload any viruses, Trojan horses, worms, logic bombs, time bombs, cancelbots, corrupted files or any other similar software, program or material which is malicious or technologically harmful or that that may damage the operation of another’s property or of the Website or our services;
- attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is stored, or any server, computer or database connected to the Website;
- remove any copyright or other proprietary notices from our Website or any of the materials contained therein;
- attack the Website via a denial-of-service attack or a distributed denial-of-service attack;
- otherwise attempt to interfere with the proper working of the Website.

**Termination**

We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.

We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.

While pornographic and adult-oriented Content is accepted, the Website reserves the right to decide whether Content is appropriate or violates these Terms of Service for reasons other than copyright infringement and violations of intellectual property rights, such as, but not limited to, obscene or defamatory material.

If you violate the letter or spirit of these Terms of Service, or otherwise create risk or possible legal exposure for us, we can terminate access to the Website or stop providing all or part of the Website to you.

Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.

All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.

**Governing Law**

These Terms shall be governed and construed in accordance with the laws of Quebec, Canada, without regard to its conflict of law provisions.

Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.

**Changes**

We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.

By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.

**Contact Us**

If you have any questions about these Terms, please contact us.